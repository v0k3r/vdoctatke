<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 05.10.17
 * Time: 18:44
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * NewsPhotos
 *
 * @ORM\Table(name="newsPhotos")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsPhotosRepository")
 */
class NewsPhotos
{

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="json_array")
     */
    private $photos = array();

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $uid;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set photos
     *
     * @param array $photos
     *
     * @return NewsPhotos
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;

        return $this;
    }

    /**
     * Get photos
     *
     * @return array
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Set uid
     *
     * @param string $uid
     *
     * @return NewsPhotos
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }
    /**
     * Add photos
     *
     * @param array $photos
     *
     * @return NewsPhotos
     */
    public function addPhotos($photos)
    {
        $this->photos=array_merge($this->photos,$photos);

        return $this;
    }
    public function removePhoto($photo){
        while (($i = array_search($photo, $this->photos)) !== false) {
            unset($this->photos[$i]);
        }
    }
}
