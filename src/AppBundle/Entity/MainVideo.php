<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 28.08.17
 * Time: 19:33
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * MainVideo
 * @ORM\Table(name="main_video")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MainVideoRepository")
 */
class MainVideo
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text",options={"collate":"utf8mb4_unicode_ci", "charset":"utf8mb4"})
     */
    protected $videoText;

    /**
     * @ORM\Column(type="string")
     */
    protected $videoId;

    /**
     * @ORM\Column(name="date", type="datetime",nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    public function __construct()
    {
        $this->date = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set videoText
     *
     * @param string $videoText
     *
     * @return MainVideo
     */
    public function setVideoText($videoText)
    {
        $this->videoText = $videoText;

        return $this;
    }

    /**
     * Get videoText
     *
     * @return string
     */
    public function getVideoText()
    {
        return $this->videoText;
    }

    /**
     * Set videoId
     *
     * @param string $videoId
     *
     * @return MainVideo
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;

        return $this;
    }

    /**
     * Get videoId
     *
     * @return string
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return MainVideo
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return MainVideo
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
    function __toString()
    {
        switch ($this->type) {
            case "marketing":
                return "Видео бизнес-плана";
                break;
            case "main":
                return "Видео кабинета клиента";
                break;
            case "training":
                return "Видео стартовое";
                break;
            case "landing":
                return "Видео лэндинга";
                break;
            default:
                return $this->type;
        }
    }
}
