<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 11.07.17
 * Time: 2:57
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dialog
 * @ORM\Table(name="dialog")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DialogRepository")
 */
class Dialog
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="dialog",cascade={"remove"})
     * @ORM\OrderBy({"id" = "ASC"})
     */
    protected $messages;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $from;
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $to;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $lastActive;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->lastActive = new \DateTime("now");

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add message
     *
     * @param \AppBundle\Entity\Message $message
     *
     * @return Dialog
     */
    public function addMessage(\AppBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \AppBundle\Entity\Message $message
     */
    public function removeMessage(\AppBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set from
     *
     * @param \AppBundle\Entity\User $from
     *
     * @return Dialog
     */
    public function setFrom(\AppBundle\Entity\User $from = null)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return \AppBundle\Entity\User
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set to
     *
     * @param \AppBundle\Entity\User $to
     *
     * @return Dialog
     */
    public function setTo(\AppBundle\Entity\User $to = null)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return \AppBundle\Entity\User
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Set lastActive
     *
     * @param \DateTime $lastActive
     *
     * @return Dialog
     */
    public function setLastActive($lastActive)
    {
        $this->lastActive = $lastActive;

        return $this;
    }

    /**
     * Get lastActive
     *
     * @return \DateTime
     */
    public function getLastActive()
    {
        return $this->lastActive;
    }

    public function getClassName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }


    public function __toString()
    {
        return $this->id.'';
    }
}
