<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 27.08.17
 * Time: 20:02
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Course
 * @ORM\Table(name="landing_event")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LandingEventRepository")
 */
class LandingEvent
{

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    protected $datetime;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    protected $weekNumber;

    /**
     * @ORM\Column(type="string")
     */
    protected $time;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return LandingEvent
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return LandingEvent
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set weekNumber
     *
     * @param integer $weekNumber
     *
     * @return LandingEvent
     */
    public function setWeekNumber($weekNumber)
    {
        $this->weekNumber = $weekNumber;

        return $this;
    }

    /**
     * Get weekNumber
     *
     * @return integer
     */
    public function getWeekNumber()
    {
        return $this->weekNumber;
    }

    /**
     * Set time
     *
     * @param string $time
     *
     * @return LandingEvent
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }
    public function __toString()
    {
       return $this->type.' '.$this->id;
    }
    public function dateToString()
    {
        if ($this->datetime!=null) {
            return $this->datetime->format('d.m.y');
        }
        else
            return $this->datetime;
    }
}
