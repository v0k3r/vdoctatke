<?php
/**
 * IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH 
 * 
 * The following source code is PROPRIETARY AND CONFIDENTIAL. Use of this source code 
 * is governed by the IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. Non-Disclosure Agreement 
 * previously entered between you and IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. 
 * 
 * By accessing, using, copying, modifying or distributing this software, you acknowledge 
 * that you have been informed of your obligations under the Agreement and agree 
 * to abide by those obligations. 
 * 
 * @author vladislav <vladislav.baymurzin@i-a-t.net>  
 */


namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class MaterialArchiveFile
 * @package AppBundle\Entity
 *  @ORM\Table(name="material_archive_files")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MaterialArchiveRepository")
 */
class MaterialArchiveFile
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="date", type="datetime")
     */

    private $date;

    /**
     * @ORM\Column(name="is_directory", type="boolean")
     */
    private $isDirectory;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="MaterialArchiveFile", mappedBy="parent", fetch="EAGER", cascade={"remove"})
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="MaterialArchiveFile", inversedBy="children", fetch="EAGER", cascade={"remove"})
     * @ORM\JoinColumn(name="parent", referencedColumnName="id")
     */
    private $parent;

    /**
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="text",options={"collate":"utf8mb4_unicode_ci", "charset":"utf8mb4"}, nullable=true)
     */
    protected $text;

    public function __construct()
    {
        $this->date = new \DateTime("now");
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return MaterialArchiveFile
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set isDirectory
     *
     * @param boolean $isDirectory
     *
     * @return MaterialArchiveFile
     */
    public function setIsDirectory($isDirectory)
    {
        $this->isDirectory = $isDirectory;

        return $this;
    }

    /**
     * Get isDirectory
     *
     * @return boolean
     */
    public function getIsDirectory()
    {
        return $this->isDirectory;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MaterialArchiveFile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return MaterialArchiveFile
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return MaterialArchiveFile
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\MaterialArchiveFile $child
     *
     * @return MaterialArchiveFile
     */
    public function addChild(\AppBundle\Entity\MaterialArchiveFile $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\MaterialArchiveFile $child
     */
    public function removeChild(\AppBundle\Entity\MaterialArchiveFile $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\MaterialArchiveFile $parent
     *
     * @return MaterialArchiveFile
     */
    public function setParent(\AppBundle\Entity\MaterialArchiveFile $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\MaterialArchiveFile
     */
    public function getParent()
    {
        return $this->parent;
    }
}
