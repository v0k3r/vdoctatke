<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 11.07.17
 * Time: 2:57
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MessageRepository")
 */
class Message
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $isReaded;
    /**
     * @ORM\Column(type="text",options={"collate":"utf8mb4_unicode_ci", "charset":"utf8mb4"})
     */
    protected $text;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $date;
    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    protected $from;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    protected $to;
    /**
     * @ORM\ManyToOne(targetEntity="Dialog", inversedBy="messages")
     */
    protected $dialog;

    public function __construct()
    {
        $this->date = new \DateTime("now");
        $this->isReaded=false;

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isReaded
     *
     * @param boolean $isReaded
     *
     * @return Message
     */
    public function setIsReaded($isReaded)
    {
        $this->isReaded = $isReaded;

        return $this;
    }

    /**
     * Get isReaded
     *
     * @return boolean
     */
    public function getIsReaded()
    {
        return $this->isReaded;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Message
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Message
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set from
     *
     * @param \AppBundle\Entity\User $from
     *
     * @return Message
     */
    public function setFrom(\AppBundle\Entity\User $from = null)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return \AppBundle\Entity\User
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set dialog
     *
     * @param \AppBundle\Entity\Dialog $dialog
     *
     * @return Message
     */
    public function setDialog(\AppBundle\Entity\Dialog $dialog = null)
    {
        $this->dialog = $dialog;

        return $this;
    }

    /**
     * Get dialog
     *
     * @return \AppBundle\Entity\Dialog
     */
    public function getDialog()
    {
        return $this->dialog;
    }
    public function dateToString(){
        return $this->date->format('d.m.Y H-i');
    }

    /**
     * Set to
     *
     * @param \AppBundle\Entity\User $to
     *
     * @return Message
     */
    public function setTo(\AppBundle\Entity\User $to = null)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return \AppBundle\Entity\User
     */
    public function getTo()
    {
        return $this->to;
    }
}
