<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 27.09.17
 * Time: 1:35
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Moderation
 *
 * @ORM\Table(name="moderation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ModerationRepository")
 */
class Moderation
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $inModeration=false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set inModeration
     *
     * @param boolean $inModeration
     *
     * @return Moderation
     */
    public function setInModeration($inModeration)
    {
        $this->inModeration = $inModeration;

        return $this;
    }

    /**
     * Get inModeration
     *
     * @return boolean
     */
    public function getInModeration()
    {
        return $this->inModeration;
    }
}
