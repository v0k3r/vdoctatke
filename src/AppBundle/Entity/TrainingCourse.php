<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 15.07.17
 * Time: 7:40
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrainingCourse
 * @ORM\Table(name="training_course")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TrainingCourseRepository")
 */
class TrainingCourse
{


    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $videoId;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $photoPath;

    protected $uploadImage;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set videoId
     *
     * @param string $videoId
     *
     * @return TrainingCourse
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;

        return $this;
    }

    /**
     * Get videoId
     *
     * @return string
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return TrainingCourse
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set photoPath
     *
     * @param string $photoPath
     *
     * @return TrainingCourse
     */
    public function setPhotoPath($photoPath)
    {
        $this->photoPath = $photoPath;

        return $this;
    }

    /**
     * Get photoPath
     *
     * @return string
     */
    public function getPhotoPath()
    {
        return $this->photoPath;
    }
    /**
     * @return mixed
     */
    public function getUploadImage()
    {
        return $this->uploadImage;
    }

    /**
     * @param mixed $uploadAvatar
     * @return TrainingCourse
     */
    public function setUploadImage($uploadImage)
    {
        $this->uploadImage = $uploadImage;
        return $this;
    }
    function __toString()
    {
        return $this->name.'';
    }
}
