<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 22.06.17
 * Time: 11:35
 */

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * News
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsRepository")
 */
class News
{

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(name="body", type="text")
     */
    private $body;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(name="date", type="datetime")
     */

    private $date;
    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $prePhotoPath;

    /**
     * @ORM\Column(type="json_array")
     */
    private $photos = array();
    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $uid;
    public function __construct()
    {
        $this->date = new \DateTime("now");
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return News
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return News
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return News
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getStringDate(){
        return $this->date->format('d.m.Y H-i');
    }

    /**
     * Set prePhotoPath
     *
     * @param string $prePhotoPath
     *
     * @return News
     */
    public function setPrePhotoPath($prePhotoPath)
    {
        $this->prePhotoPath = $prePhotoPath;

        return $this;
    }

    /**
     * Get prePhotoPath
     *
     * @return string
     */
    public function getPrePhotoPath()
    {
        return $this->prePhotoPath;
    }

    /**
     * Set photos
     *
     * @param array $photos
     *
     * @return News
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;

        return $this;
    }

    /**
     * Get photos
     *
     * @return array
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Set uid
     *
     * @param string $uid
     *
     * @return News
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }
    public function removePhoto($photo){
        while (($i = array_search($photo, $this->photos)) !== false) {
            unset($this->photos[$i]);
        }
    }
    /**
     * Add photos
     *
     * @param array $photos
     *
     * @return News
     */
    public function addPhotos($photos)
    {
        $this->photos=array_merge($this->photos,$photos);

        return $this;
    }
    public function getStringDateNoTime(){
        return $this->date->format('d.m.Y');
    }
}
