<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 15.07.17
 * Time: 7:40
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BusinessOnlineCourse
 * @ORM\Table(name="business_online_course")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BusinessOnlineCourseRepository")
 */
class BusinessOnlineCourse
{


    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $videoId;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $photoPath;

    protected $uploadImage;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set videoId
     *
     * @param string $videoId
     *
     * @return BusinessOnlineCourse
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;

        return $this;
    }

    /**
     * Get videoId
     *
     * @return string
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return BusinessOnlineCourse
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set photoPath
     *
     * @param string $photoPath
     *
     * @return BusinessOnlineCourse
     */
    public function setPhotoPath($photoPath)
    {
        $this->photoPath = $photoPath;

        return $this;
    }

    /**
     * Get photoPath
     *
     * @return string
     */
    public function getPhotoPath()
    {
        return $this->photoPath;
    }
    /**
     * @return mixed
     */
    public function getUploadImage()
    {
        return $this->uploadImage;
    }

    /**
     * @param mixed $uploadAvatar
     * @return BusinessOnlineCourse
     */
    public function setUploadImage($uploadImage)
    {
        $this->uploadImage = $uploadImage;
        return $this;
    }
    function __toString()
    {
        return $this->name.'';
    }
}
