<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 14.06.17
 * Time: 19:00
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 * @ORM\Table(name="simple_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @UniqueEntity(fields="tel", message="Телефон уже использован")
 * @UniqueEntity(fields="telegram", message="Телеграм уже использован")
 * @UniqueEntity(fields="vk", message="vk уже использован")
 * @UniqueEntity(fields="whatsup", message="whatsApp уже использован")
 * @UniqueEntity(fields="email", message="Email уже занят")
 * @UniqueEntity(fields="username", message="Username уже занят")
 * @UniqueEntity(fields="landingName", message="Лэндинг уже занят")
 * @UniqueEntity(fields="sureone_id", message="Реферальная ссылка уже используется")
 */
class User implements UserInterface
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @Assert\Email(
     *     message = "Email '{{ value }}' не валидный",
     * )
     */
    private $email;
    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true,nullable=true)
     */
    private $username;

    /**
     * @var string
     * @ORM\Column(name="pass", type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="json_array")
     */
    private $roles = array();

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $first_name;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $last_name;

    /**
     * @ORM\Column(type="string",nullable=true)
     * @Assert\Regex(
     *     pattern="^((8|\+)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,12}$^",
     *     message="Неверный формат мобильного телефона"
     * )
     */
    protected $tel;

    /**
     * @ORM\Column(type="string",nullable=true)
     * @Assert\Regex(
     *     pattern="^((8|\+)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,12}$^",
     *     message="Неверный формат телеграм"
     * )
     */
    protected $telegram;

    /**
     * @ORM\Column(type="string",nullable=true)
     * @Assert\Regex(
     *     pattern="/(id\d|[a-zA-z][a-zA-Z0-9_.]{2,})/",
     *     message="Неверный формат vk, напишите ваш VK ID vk.com/??"
     * )
     */
    protected $vk;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $country;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $city;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $sureone_id;
    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="parent", fetch="EXTRA_LAZY")
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="children")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id")
     */
    private $parent;


    /**
     * @ORM\Column(type="string",nullable=true)
     * @Assert\Regex(
     *     pattern="^((8|\+)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,12}$^",
     *     message="Неверный формат whatsApp"
     * )
     */
    protected $whatsup;

    /**
     * @ORM\Column(type="string", nullable=true, unique=true)
     * @Assert\Regex(
     *     pattern="/^[A-Za-z0-9-_]{3,15}$/",
     *     message="Неверный формат лэндинга"
     * )
     */
    protected $landingName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $photoPath;
    /**
     * @ORM\Column(type="integer")
     */
    protected $step;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $views;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $ip = array();

    /**
     * @ORM\ManyToOne(targetEntity="Course")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $course;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $lastNewsVisit;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $lastTrainingVisit;

    /**
     * @ORM\ManyToOne(targetEntity="TrainingCourse")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $trcourse;

    /**
     * @ORM\ManyToOne(targetEntity="BusinessOnlineCourse")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $bcourse;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $registratedAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $confirmed;

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    public function getRoles()
    {
        $roles = $this->roles;


        return array_unique($roles);
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;

        // allows for chaining
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return User
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set telegram
     *
     * @param string $telegram
     *
     * @return User
     */
    public function setTelegram($telegram)
    {
        $this->telegram = $telegram;

        return $this;
    }

    /**
     * Get telegram
     *
     * @return string
     */
    public function getTelegram()
    {
        return $this->telegram;
    }

    /**
     * Set vk
     *
     * @param string $vk
     *
     * @return User
     */
    public function setVk($vk)
    {
        $this->vk = $vk;

        return $this;
    }

    /**
     * Get vk
     *
     * @return string
     */
    public function getVk()
    {
        return $this->vk;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set sureoneId
     *
     * @param string $sureoneId
     *
     * @return User
     */
    public function setSureoneId($sureoneId)
    {
        $this->sureone_id = $sureoneId;

        return $this;
    }

    /**
     * Get sureoneId
     *
     * @return string
     */
    public function getSureoneId()
    {
        return $this->sureone_id;
    }

    /**
     * Set whatsup
     *
     * @param string $whatsup
     *
     * @return User
     */
    public function setWhatsup($whatsup)
    {
        $this->whatsup = $whatsup;

        return $this;
    }

    /**
     * Get whatsup
     *
     * @return string
     */
    public function getWhatsup()
    {
        return $this->whatsup;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\User $parent
     *
     * @return User
     */
    public function setParent(\AppBundle\Entity\User $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\User
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->email;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->step=1;
        $this->views = 0;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\User $child
     *
     * @return User
     */
    public function addChild(\AppBundle\Entity\User $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\User $child
     */
    public function removeChild(\AppBundle\Entity\User $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set landingName
     *
     * @param string $landingName
     *
     * @return User
     */
    public function setLandingName($landingName)
    {
        $this->landingName = $landingName;

        return $this;
    }

    /**
     * Get landingName
     *
     * @return string
     */
    public function getLandingName()
    {
        return $this->landingName;
    }

    /**
     * Set photoPath
     *
     * @param string $photoPath
     *
     * @return User
     */
    public function setPhotoPath($photoPath)
    {
        $this->photoPath = $photoPath;

        return $this;
    }

    /**
     * Get photoPath
     *
     * @return string
     */
    public function getPhotoPath()
    {
        return $this->photoPath;
    }


    /**
     * Set step
     *
     * @param integer $step
     *
     * @return User
     */
    public function setStep($step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Get step
     *
     * @return integer
     */
    public function getStep()
    {
        return $this->step;
    }

    protected $uploadAvatar;

    /**
     * @return mixed
     */
    public function getUploadAvatar()
    {
        return $this->uploadAvatar;
    }

    /**
     * @param mixed $uploadAvatar
     * @return User
     */
    public function setUploadAvatar($uploadAvatar)
    {
        $this->uploadAvatar = $uploadAvatar;
        return $this;
    }



    /**
     * Set views
     *
     * @param integer $views
     *
     * @return User
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set ip
     *
     * @param array $ip
     *
     * @return User
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return array
     */
    public function getIp()
    {
        return $this->ip;
    }

    public function getClassName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * Set course
     *
     * @param \AppBundle\Entity\Course $course
     *
     * @return User
     */
    public function setCourse(\AppBundle\Entity\Course $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \AppBundle\Entity\Course
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set trcourse
     *
     * @param \AppBundle\Entity\TrainingCourse $trcourse
     *
     * @return User
     */
    public function setTrcourse(\AppBundle\Entity\TrainingCourse $trcourse = null)
    {
        $this->trcourse = $trcourse;

        return $this;
    }

    /**
     * Get trcourse
     *
     * @return \AppBundle\Entity\TrainingCourse
     */
    public function getTrcourse()
    {
        return $this->trcourse;
    }

    /**
     * Set lastNewsVisit
     *
     * @param \DateTime $lastNewsVisit
     *
     * @return User
     */
    public function setLastNewsVisit($lastNewsVisit)
    {
        $this->lastNewsVisit = $lastNewsVisit;

        return $this;
    }

    /**
     * Get lastNewsVisit
     *
     * @return \DateTime
     */
    public function getLastNewsVisit()
    {
        return $this->lastNewsVisit;
    }

    /**
     * Set lastTrainingVisit
     *
     * @param \DateTime $lastTrainingVisit
     *
     * @return User
     */
    public function setLastTrainingVisit($lastTrainingVisit)
    {
        $this->lastTrainingVisit = $lastTrainingVisit;

        return $this;
    }

    /**
     * Get lastTrainingVisit
     *
     * @return \DateTime
     */
    public function getLastTrainingVisit()
    {
        return $this->lastTrainingVisit;
    }

    /**
     * Set bcourse
     *
     * @param \AppBundle\Entity\BusinessOnlineCourse $bcourse
     *
     * @return User
     */
    public function setBcourse(\AppBundle\Entity\BusinessOnlineCourse $bcourse = null)
    {
        $this->bcourse = $bcourse;

        return $this;
    }

    /**
     * Get bcourse
     *
     * @return \AppBundle\Entity\BusinessOnlineCourse
     */
    public function getBcourse()
    {
        return $this->bcourse;
    }

    /**
     * Set registratedAt
     *
     * @param \DateTime $registratedAt
     *
     * @return User
     */
    public function setRegistratedAt($registratedAt)
    {
        $this->registratedAt = $registratedAt;

        return $this;
    }

    /**
     * Get registratedAt
     *
     * @return \DateTime
     */
    public function getRegistratedAt()
    {
        return $this->registratedAt;
    }

    /**
     * Set confirmed
     *
     * @param \AppBundle\Entity\User $confirmed
     *
     * @return User
     */
    public function setConfirmed(\AppBundle\Entity\User $confirmed = null)
    {
        $this->confirmed = $confirmed;

        return $this;
    }

    /**
     * Get confirmed
     *
     * @return \AppBundle\Entity\User
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }
}
