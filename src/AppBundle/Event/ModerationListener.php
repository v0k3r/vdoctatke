<?php

namespace AppBundle\Event;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 27.09.17
 * Time: 6:32
 */
class ModerationListener
{
    protected $requestStack;
    protected $em;
    protected $container;
    public function __construct(ContainerInterface $container,RequestStack $requestStack) {
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->requestStack = $requestStack;
        $this->container=$container;
    }
    public function onModerationRequest(GetResponseEvent $event)
    {
        $request = $this->requestStack->getCurrentRequest();
        $session = $request->getSession();


        $moderation=$this->em->getRepository('AppBundle:Moderation')->findOneBy([]);
        if ($moderation->getInModeration()) {
            $request = $this->requestStack->getCurrentRequest();
            $route=$request->attributes->get('_route');
            $url = $request->getUri();

            $isTrue=$route=='show_landing'|| $route=='homepage'|| $route=='user_registration'|| $route=='login' || $route=='app_security_dispatch'|| strpos($url, '/admin/main') !== false;

            $user= $this->container->get('security.token_storage')->getToken()->getUser();
            if ($user instanceof User){
                $role=$user->getRoles();
                $userAccess=in_array('ROLE_ADMIN', $role);
            }
            else{
               $userAccess=true;
            }

            if (!$isTrue && !$userAccess) {
                if(strpos($url, 'auth/login') !== false){
                    $isDialogs=0;
                }
                else{
                    $isDialogs=1;
                }
                $session->set('moderation', '1');
                $engine = $this->container->get('templating');

                $content = $engine->render('pages/moderation/moderation.html.twig',[
                   'isMessages'=>$isDialogs
                ]);
                $event->setResponse(new Response($content));
                $event->stopPropagation();
            }
        }
        else{
            $session->set('moderation', '0');
        }
    }
}