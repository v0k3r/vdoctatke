<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 14.07.17
 * Time: 13:20
 */

namespace AppBundle\Service;


class ImageGrayscaleService
{
    /**
     * @param $filename
     * @param $resultName
     * @return boolean
     */
    public function grayscaleFilter($filename, $resultName=null){
        if ($resultName==null)
            $resultName=$filename;

        $imgSize = getimagesize($filename);
        $width = $imgSize[0];
        $height = $imgSize[1];
        if ($width>=$height)
            $width=$height;
        else
            $height=$width;

        $img = imageCreate($width,$height);
        for ($color = 0; $color <= 255; $color++) {
            imageColorAllocate($img, $color, $color, $color);
        }

        $exploded = explode('.',$filename);
        $ext = $exploded[count($exploded) - 1];
        if (preg_match('/jpg|jpeg/i',$ext)) {
            $img2 = imagecreatefromjpeg($filename);
            imageCopyMerge($img,$img2,0,0,0,0, $width, $height, 100);
            imagejpeg($img, $resultName);
        }
        else if (preg_match('/png/i',$ext)) {
            $img2 = imagecreatefrompng($filename);
            imageCopyMerge($img,$img2,0,0,0,0, $width, $height, 100);
            imagepng($img, $resultName);
        }
        else
            return 0;
        imagedestroy($img);
        return 1;
    }
}