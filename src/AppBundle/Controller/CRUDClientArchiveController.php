<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 01.10.17
 * Time: 4:53
 */

namespace AppBundle\Controller;

use AppBundle\DTO\CreateFolder;
use AppBundle\Entity\ArchiveFile;
use AppBundle\Entity\ClientArchiveFile;
use AppBundle\Form\CreateArchiveFileType;
use AppBundle\Form\CreateClientArchiveFileType;
use AppBundle\Form\CreateFolderType;
use AppBundle\Repository\ArchiveRepository;
use AppBundle\Repository\ClientArchiveRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CoreController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
class CRUDClientArchiveController extends CoreController
{
    /**
     * @Route("/admin/main/clientarchive/{parent}", name="admin_show_clientarchive")
     * @param null $parent
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction($parent = null, Request $request, EntityManagerInterface $entityManager)
    {
        $parent = (!$parent) ? null : $parent;
        $current = $parent;
        $archiveRepository = $entityManager->getRepository("AppBundle:ClientArchiveFile");
//        $files = $archiveRepository->findAll();
        $children = [];
        $prev = null;
        if ($parent != null) {
            /** @var ClientArchiveFile $currentFolder */
            $currentFolder = $archiveRepository->find($current);
            $children = $currentFolder->getChildren();
            if (is_null($currentFolder->getParent())) {
                $prev = 0;
            } else {
                $prev = $currentFolder->getParent()->getId();
            }
        } else {
            $children = $archiveRepository->findBy([
                'parent' => null
            ]);
        }

        $archiveFolderDTO = new CreateFolder();
        $formCreateFolder = $this->createForm(CreateFolderType::class, $archiveFolderDTO, [
            'action' => $this->generateUrl('admin-clientcreate_folder', ['parent' => $parent]),
            'method' => "POST"
        ]);

        $formCreateFolder->handleRequest($request);
        if (is_array($children)){
            $children=array_reverse($children);
        }
        elseif (count($children)>1){
            $children =array_reverse($children->getValues());
        }
//        $children = (is_array($children)) ? array_reverse($children) : $children;

        return $this->render('CRUD/clientarchive.html.twig', array(
            'base_template' => $this->getBaseTemplate(),
            'admin_pool' => $this->container->get('sonata.admin.pool'),
            'blocks' => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'),
            'formFolderCreate' => $formCreateFolder->createView(),
            'children' => $children,
            'root' => (is_null($parent)) ? true : false,
            'prev' => $prev,
            'parent'=>$parent
        ));

    }
    /**
     * @Route("/admin/main/app/clientarchivefile/create/{parent}", name="admin-clientcreate_file")
     * @param null $parent
     * @param Request $request
     * @return string
     */
    public function createAction($parent = null, Request $request, EntityManagerInterface $entityManager){
        /** @var ClientArchiveRepository $archiveRepository */
        $archiveRepository = $entityManager->getRepository("AppBundle:ClientArchiveFile");
        $file=new ClientArchiveFile();
        $parentFolder=$parent;
        $formCreateFile = $this->createForm(CreateClientArchiveFileType::class, $file);
        $formCreateFile->handleRequest($request);
        if ($formCreateFile->isSubmitted() && $formCreateFile->isValid()) {
            if ($file->getName()==''){
                $file->setName('Новый файл');
            }
            if ($parent!=null){
                $parentFolder=$entityManager->getRepository('AppBundle:ClientArchiveFile')->find($parent);
            }
            $file->setType('Файл')->setIsDirectory(false)->setParent($parentFolder);
            $entityManager->persist($file);
            $entityManager->flush();

            return $this->redirectToRoute('admin_show_clientarchive', [
                'parent' => $parent
            ]);
        }
        return $this->render('CRUD/create-clientarchivefile.html.twig',[
            'form'=>$formCreateFile->createView()
        ]);
    }

    /**
     * @Route("/admin/main/app/clientfile/remove/{id}", name="admin-clientremove_file")
     * @param $id
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function removeItem($id, Request $request, EntityManagerInterface $entityManager)
    {
        /** @var ArchiveRepository $archiveRepository */
        $archiveRepository = $entityManager->getRepository("AppBundle:ClientArchiveFile");
        /** @var ClientArchiveFile $item */
        $item = $archiveRepository->find($id);
        $parent = $item->getParent();
        $item->setParent(null);
        if ($parent!=null) {
            $parent->removeChild($item);
            $entityManager->persist($parent);
        }
        $entityManager->remove($item);
        $entityManager->flush();
        $redirect = null;
        if (!is_null($parent)) {
            $redirect = $parent->getId();
        }
        return $this->redirectToRoute('admin_show_clientarchive', [
            'parent' =>$redirect
        ]);
    }

    /**
     * @Route("/admin/main/app/clientarchive/redactfile/{id}", name="admin-clientredact_file")
     * @param $id
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function redactFile($id, Request $request, EntityManagerInterface $entityManager)
    {
        /** @var ClientArchiveRepository $archiveRepository */
        $archiveRepository = $entityManager->getRepository("AppBundle:ClientArchiveFile");
        $file=$archiveRepository->find($id);

        $formCreateFile = $this->createForm(CreateClientArchiveFileType::class, $file);
        $formCreateFile->handleRequest($request);
        if ($formCreateFile->isSubmitted() && $formCreateFile->isValid()) {
            if ($file->getName()==''){
                $file->setName('Новый файл');
            }
            $entityManager->persist($file);
            $entityManager->flush();
            $parent=$file->getParent();
            if ($parent==null){
                $id=0;
            }
            else{
                $id=$parent->getId();
            }
            return $this->redirectToRoute('admin_show_clientarchive', [
                'parent' => $id
            ]);
        }
        return $this->render('CRUD/create-clientarchivefile.html.twig',[
            'form'=>$formCreateFile->createView()
        ]);
    }
    /**
     * @Route("/admin/main/app/clientfolder/{parent}", name="admin-clientcreate_folder")
     * @param null $parent
     * @param Request $request
     * @return string
     */
    public function createFolder($parent = null, Request $request, EntityManagerInterface $entityManager)
    {
        $archiveRepository = $entityManager->getRepository("AppBundle:ClientArchiveFile");
        $archiveFolderDTO = new CreateFolder();
        $formCreateFolder = $this->createForm(CreateFolderType::class, $archiveFolderDTO);
        $formCreateFolder->handleRequest($request);
        if ($formCreateFolder->isSubmitted() && $formCreateFolder->isValid()) {
            $folder = new ClientArchiveFile();
            if (!$parent) {
                $folder->setParent(null);
            } else {
                $parentEntity = $archiveRepository->find($parent);
                $folder->setParent($parentEntity);
            }
            $folder->setIsDirectory(true);
            if ($archiveFolderDTO->name==''){
                $folder->setName("Новая папка");
            }
            else{
                $folder->setName($archiveFolderDTO->name);
            }

            $folder->setType('D');
            $entityManager->persist($folder);
            $entityManager->flush();
            return $this->redirectToRoute('admin_show_clientarchive', [
                'parent' => $folder->getId()
            ]);
        }
        return $this->redirectToRoute('admin_show_clientarchive', [
            'parent' => $parent
        ]);
    }

    /**
     * @Route("/admin/main/app/clientarchive/showfile/{id}", name="admin-clientshow_file")
     * @param $id
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function showFile($id, Request $request, EntityManagerInterface $entityManager)
    {
        /** @var ClientArchiveRepository $archiveRepository */
        $archiveRepository = $entityManager->getRepository("AppBundle:ClientArchiveFile");
        $file=$archiveRepository->find($id);
        return $this->render("CRUD/show-client-archive.html.twig",[
            'file'=>$file
        ]);
    }
}