<?php
/**
 * IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH 
 * 
 * The following source code is PROPRIETARY AND CONFIDENTIAL. Use of this source code 
 * is governed by the IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. Non-Disclosure Agreement 
 * previously entered between you and IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. 
 * 
 * By accessing, using, copying, modifying or distributing this software, you acknowledge 
 * that you have been informed of your obligations under the Agreement and agree 
 * to abide by those obligations. 
 * 
 * @author vladislav <vladislav.baymurzin@i-a-t.net>  
 */


namespace AppBundle\Controller;


use AppBundle\DTO\CreateArchiveFile;
use AppBundle\DTO\CreateFolder;
use AppBundle\Entity\ArchiveFile;
use AppBundle\Entity\User;
use AppBundle\Form\CreateArchiveFileType;
use AppBundle\Form\CreateFolderType;
use AppBundle\Repository\ArchiveRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ArchiveController extends Controller
{

    /**
     * @Route("/cabinet/archive/{parent}", name="show_archive")
     * @param null $parent
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleArchive($parent = null, Request $request, EntityManagerInterface $entityManager)
    {
        $parent = (!$parent) ? null : $parent;
        $current = $parent;
        $archiveRepository = $entityManager->getRepository("AppBundle:ArchiveFile");
//        $files = $archiveRepository->findAll();
        $children = [];
        $prev = null;
        if ($parent != null) {
            /** @var ArchiveFile $currentFolder */
            $currentFolder = $archiveRepository->find($current);
            $children = $currentFolder->getChildren();
            if (is_null($currentFolder->getParent())) {
                $prev = 0;
            } else {
                $prev = $currentFolder->getParent()->getId();
            }
        } else {
            $children = $archiveRepository->findBy([
                'parent' => null
            ]);
        }

        $archiveFolderDTO = new CreateFolder();
        $formCreateFolder = $this->createForm(CreateFolderType::class, $archiveFolderDTO, [
            'action' => $this->generateUrl('create_folder', ['parent' => $parent]),
            'method' => "POST"
        ]);

        $formCreateFolder->handleRequest($request);
        if (is_array($children)){
            $children=array_reverse($children);
        }
        elseif (count($children)>1){
            $children =array_reverse($children->getValues());
        }
//        $children = (is_array($children)) ? array_reverse($children) : $children;
        return $this->render('pages/archive/archive.html.twig', [
            'formFolderCreate' => $formCreateFolder->createView(),
            'children' => $children,
            'root' => (is_null($parent)) ? true : false,
            'prev' => $prev,
            'parent'=>$parent
        ]);
    }

    /**
     * @Route("/cabinet/folder/{parent}", name="create_folder")
     * @param null $parent
     * @param Request $request
     * @return string
     */
    public function createFolder($parent = null, Request $request, EntityManagerInterface $entityManager)
    {
        $archiveRepository = $entityManager->getRepository("AppBundle:ArchiveFile");
        $archiveFolderDTO = new CreateFolder();
        $formCreateFolder = $this->createForm(CreateFolderType::class, $archiveFolderDTO);
        $formCreateFolder->handleRequest($request);
        if ($formCreateFolder->isSubmitted() && $formCreateFolder->isValid()) {
            $folder = new ArchiveFile();
            if (!$parent) {
                $folder->setParent(null);
            } else {
                $parentEntity = $archiveRepository->find($parent);
                $folder->setParent($parentEntity);
            }
            $folder->setIsDirectory(true);
            if ($archiveFolderDTO->name==''){
                $folder->setName("Новая папка");
            }
            else{
                $folder->setName($archiveFolderDTO->name);
            }

            $folder->setType('D');
            $entityManager->persist($folder);
            $entityManager->flush();
            return $this->redirectToRoute('show_archive', [
                'parent' => $folder->getId()
            ]);
        }
        return $this->redirectToRoute('show_archive', [
            'parent' => $parent
        ]);
    }

    /**
     * @Route("/cabinet/file/create/{parent}", name="create_file")
     * @param null $parent
     * @param Request $request
     * @return string
     */
    public function createFile($parent = null, Request $request, EntityManagerInterface $entityManager)
    {
        /** @var ArchiveRepository $archiveRepository */
        $archiveRepository = $entityManager->getRepository("AppBundle:ArchiveFile");
       $file=new ArchiveFile();
       $parentFolder=$parent;
        $formCreateFile = $this->createForm(CreateArchiveFileType::class, $file);
        $formCreateFile->handleRequest($request);
        if ($formCreateFile->isSubmitted() && $formCreateFile->isValid()) {
            if ($file->getName()==''){
                $file->setName('Новый файл');
            }
            if ($parent!=null){
                $parentFolder=$entityManager->getRepository('AppBundle:ArchiveFile')->find($parent);
            }
            $file->setType('Файл')->setIsDirectory(false)->setParent($parentFolder);
            $entityManager->persist($file);
            $entityManager->flush();

            return $this->redirectToRoute('show_archive', [
                'parent' => $parent
            ]);
        }
       return $this->render('pages/archive/create-file.html.twig',[
           'form'=>$formCreateFile->createView()
       ]);
    }

    /**
     * @Route("/cabinet/file/remove/{id}", name="remove_file")
     * @param $id
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function removeItem($id, Request $request, EntityManagerInterface $entityManager)
    {
        /** @var ArchiveRepository $archiveRepository */
        $archiveRepository = $entityManager->getRepository("AppBundle:ArchiveFile");
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        if (!in_array("ROLE_ADMIN", $currentUser->getRoles())) {
            return $this->createAccessDeniedException("no access");
        }
        /** @var ArchiveFile $item */
        $item = $archiveRepository->find($id);
        $parent = $item->getParent();
        $item->setParent(null);
        if ($parent!=null) {
            $parent->removeChild($item);
            $entityManager->persist($parent);
        }
        $entityManager->remove($item);
        $entityManager->flush();
        $redirect = null;
        if (!is_null($parent)) {
            $redirect = $parent->getId();
        }
        return $this->redirectToRoute('show_archive', [
            'parent' =>$redirect
        ]);
    }
    /**
     * @Route("/cabinet/archive/showfile/{id}", name="show_file")
     * @param $id
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function showFile($id, Request $request, EntityManagerInterface $entityManager)
    {
        /** @var ArchiveRepository $archiveRepository */
        $archiveRepository = $entityManager->getRepository("AppBundle:ArchiveFile");
        $file=$archiveRepository->find($id);
        return $this->render("pages/archive/file-list.html.twig",[
            'file'=>$file
        ]);
    }
    /**
     * @Route("/cabinet/archive/redactfile/{id}", name="redact_file")
     * @param $id
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function redactFile($id, Request $request, EntityManagerInterface $entityManager)
    {
        /** @var ArchiveRepository $archiveRepository */
        $archiveRepository = $entityManager->getRepository("AppBundle:ArchiveFile");
        $file=$archiveRepository->find($id);

        $formCreateFile = $this->createForm(CreateArchiveFileType::class, $file);
        $formCreateFile->handleRequest($request);
        if ($formCreateFile->isSubmitted() && $formCreateFile->isValid()) {
            if ($file->getName()==''){
                $file->setName('Новый файл');
            }
            $entityManager->persist($file);
            $entityManager->flush();
            $parent=$file->getParent();
            if ($parent==null){
                $id=0;
            }
            else{
                $id=$parent->getId();
            }
            return $this->redirectToRoute('show_archive', [
                'parent' => $id
            ]);
        }
        return $this->render('pages/archive/create-file.html.twig',[
            'form'=>$formCreateFile->createView()
        ]);
    }

}