<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 30.01.17
 * Time: 20:34
 */

namespace AppBundle\Controller;


use AppBundle\Entity\News;
use AppBundle\Entity\NewsPhotos;
use AppBundle\Entity\User;
use AppBundle\Form\NewsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Session\Session;

class NewsController extends Controller
{
    /**
     * @Route("/create/news", name="create_news")
     */

    public function createNewsAction(Request $request)
    {
        $session = new Session();
        $news = new News();
        $session->set('news', $news);
        $em = $this->getDoctrine()->getManager();
        $news->setUid(md5(uniqid()));
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $news->setUser($this->getUser());
            $news->setPrePhotoPath(null);
            /**
             *@var NewsPhotos $photos
             */
            $photos=$em->getRepository('AppBundle:NewsPhotos')->findOneByUid($news->getUid());
            $news->setBody(htmlspecialchars($news->getBody()));
            $news->setBody(str_replace("\n", ' ', $news->getBody()));
            if ($photos!=null) {
                $news->setPhotos($photos->getPhotos());
                $em->remove($photos);
            }
            if($news->getTitle()==""){
                $news->setTitle("Новость от ".$news->getStringDateNoTime());
            }
            $em->persist($news);
            $em->flush();
            return $this->redirectToRoute('news_list');
        }
        return $this->render(
            'pages/news/create.html.twig',
            array('form' => $form->createView()

            )
        );

    }

    /**
     * @Route("/cabinet/news", name="news_list")
     */
    public function NewsListAction(Request $request)
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();
        $timezone = new \DateTimeZone('Europe/Moscow');
        $date = new \DateTime('now', $timezone);
        $user->setLastNewsVisit($date);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        $news = $em->getRepository("AppBundle:News")->findBy([], ['id' => 'DESC']);;
        return $this->render(
            'pages/news/list.html.twig',
            array(
                'news' => $news

            )
        );
    }

    /**
     * @Route("/cabinet/news/update/{id}", name="news_update")
     */
    public function updateNews(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        /**
         * @var News $news
         */
        $news = $em->getRepository('AppBundle:News')->findOneById($id);

        if ($news != null) {
            $news->setPrePhotoPath(null);
            $news->setBody(htmlspecialchars_decode($news->getBody()));
            $form = $this->createForm(NewsType::class, $news);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {


                $news->setBody(htmlspecialchars($news->getBody()));
                $news->setBody(str_replace("\n", ' ', $news->getBody()));

                $em = $this->getDoctrine()->getManager();
                $em->persist($news);
                $em->flush();
                return $this->redirectToRoute('news_list');
            }


            return $this->render(
                'pages/news/update.html.twig',
                array(
                    'form' => $form->createView(),
                    'news' => $news

                )
            );
        } else return $this->render('pages/news/news.404.html.twig');
    }

    /**
     * @Route("/cabinet/news/delete/{id}", name="news_delete")
     */
    public function deleteNews(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $role = $user->getRoles();
        if (in_array('ROLE_ADMIN', $role)) {
            $deletePost = $em->getRepository('AppBundle:News')->findOneById($id);
            $em->remove($deletePost);
            $em->flush();
            return new JsonResponse(['status' => 200]);
        } else  return new JsonResponse(['status' => 403]);
    }

    /**
     * @Route("/cabinet/news/upload/{uid}", name="news_upload_image")
     */
    public function uploadNewsImage(Request $request,$uid)
    {
        $em=$this->getDoctrine()->getManager();
        $photos=$em->getRepository('AppBundle:NewsPhotos')->findOneByUid($uid);
        if ($photos==null){
            $photos=new NewsPhotos();
            $photos->setUid($uid);
        }

        $user = $this->getUser();
        $image = $request->request->get('image');

        if ($base64Content = $image) {
            $dat = preg_split("/,/", $base64Content);

            if (($fileData = base64_decode($dat[1])) === false) {
                $response = new JsonResponse(
                    array(
                        'message' => "Base64 decoding error."
                    ), 400);

                return $response;
            }
            $fileName=md5(uniqid()) . "-" .$user->getId().".jpeg";

                $photos->addPhotos([$fileName]);
                $em->persist($photos);
                $em->flush();
            $filePath = "./newsPhotos/" . $fileName;
            if (file_put_contents($filePath, $fileData)) {

                return new JsonResponse(array('fileName' => $fileName), 200);
            }
        }
        return new Response('0');
    }
    /**
     * @Route("/cabinet/news/uploadremove/{uid}/newsPhotos/{image}", name="news_remove_upload_image")
     */
    public function removeUploadNewsImage(Request $request,$uid,$image)
    {
        $em=$this->getDoctrine()->getManager();
        /**
         * @var NewsPhotos $photos
         */
        $photos=$em->getRepository('AppBundle:NewsPhotos')->findOneByUid($uid);
        $photos->removePhoto($image);
        $em->persist($photos);
        $em->flush();
        return new Response('1');
    }
    /**
     * @Route("/cabinet/news/uploadrem/{uid}/newsPhotos/{image}", name="news_remove_upload_img")
     */
    public function removeUploadNewsImg(Request $request,$uid,$image)
    {
        $em=$this->getDoctrine()->getManager();
        /**
         * @var NewsPhotos $photos
         */
        $photos=$em->getRepository('AppBundle:News')->findOneByUid($uid);
        $photos->removePhoto($image);
        $em->persist($photos);
        $em->flush();
        return new Response('1');
    }
    /**
     * @Route("/cabinet/news/upl/{uid}", name="news_upload_img")
     */
    public function uploadNewsImg(Request $request,$uid)
    {
        $em=$this->getDoctrine()->getManager();
        /**
         * @var News $photos
         */
        $photos=$em->getRepository('AppBundle:News')->findOneByUid($uid);
        $user = $this->getUser();
        $image = $request->request->get('image');

        if ($base64Content = $image) {
            $dat = preg_split("/,/", $base64Content);

            if (($fileData = base64_decode($dat[1])) === false) {
                $response = new JsonResponse(
                    array(
                        'message' => "Base64 decoding error."
                    ), 400);

                return $response;
            }
            $fileName=md5(uniqid()) . "-" .$user->getId().".jpeg";

            $photos->addPhotos([$fileName]);
            $em->persist($photos);
            $em->flush();
            $filePath = "./newsPhotos/" . $fileName;
            if (file_put_contents($filePath, $fileData)) {

                return new JsonResponse(array('fileName' => $fileName), 200);
            }
        }
        return new Response('0');
    }
}