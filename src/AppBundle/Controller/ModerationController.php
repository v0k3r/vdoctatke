<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 27.09.17
 * Time: 0:29
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Yaml\Yaml;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
class ModerationController extends Controller
{

    public function checkModeration()
    {
        $value = Yaml::parse(file_get_contents('../app/config/parameters.yml'));
        $paths = array(dirname(__DIR__).'/Entity');
        $isDevMode = false;
        $dbParams = array(
            'driver'   => 'pdo_mysql',
            'user'     => $value['parameters']['database_user'],
            'password' => $value['parameters']['database_password'],
            'dbname'   => $value['parameters']['database_name'],
        );

        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
        $entityManager = EntityManager::create($dbParams, $config);

        $queryBuilder =$entityManager->getConnection()->createQueryBuilder();
        $moderation=$queryBuilder
            ->select('in_moderation')
            ->from('moderation')
            ->setMaxResults(1)
            ->execute()
            ->fetch()

        ;

//        $moderation=$queryBuilder->getQuery()->setMaxResults(1)->getSingleResult();
        return $moderation['in_moderation'];
    }
}