<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 15.07.17
 * Time: 7:58
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class GeneralCoursesController extends Controller
{

    /**
     * @Route("/cabinet/courses", name="cabinet_courses")
     */
    public function showCoursesAction(Request $request)
    {   $coursesRepository = $this->getDoctrine()->getRepository('AppBundle:BusinessOnlineCourse');
        $coursesOnline = $coursesRepository->findAll();
        $coursesRepository = $this->getDoctrine()->getRepository('AppBundle:Course');
        $courses = $coursesRepository->findAll();
        return $this->render('pages/generalCourses/courses.online.html.twig', [
            'courses' => $courses,
            "coursesOnline"=>$coursesOnline
        ]);
    }
    /**
     * @Route("/cabinet/training/courses", name="training_courses")
     */
    public function showTrainingCoursesAction(Request $request)
    {
        $coursesRepository = $this->getDoctrine()->getRepository('AppBundle:TrainingCourse');
        $zCoursesRepository = $this->getDoctrine()->getRepository('AppBundle:ZorikTrainingCourse');

        $expressCourses=$this->getDoctrine()->getRepository('AppBundle:TypedVideoCourse')->findBy(["type"=>"tiki_express"],["id"=>"ASC"]);
        $courses = $coursesRepository->findAll();
        $zorikCourses=$zCoursesRepository->findAll();
        return $this->render('pages/generalCourses/courses.html.twig', [
            'courses' => $courses,
            'zcourses'=>$zorikCourses,
            'ecourses'=>$expressCourses
        ]);
    }
    /**
     * @Route("/cabinet/training/newcourses", name="new_courses")
     */
    public function courseList(Request $request)
    {
        $coursesRepository = $this->getDoctrine()->getRepository('AppBundle:BusinessOnlineCourse');
        $coursesOnline = $coursesRepository->findAll();
        $coursesRepository = $this->getDoctrine()->getRepository('AppBundle:Course');
        $courses = $coursesRepository->findAll();
        return $this->render('pages/generalCourses/courses.online.html.twig', [
            'courses' => $courses,
            "coursesOnline"=>$coursesOnline
        ]);
    }
}