<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 07.03.17
 * Time: 10:55
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Dialog;
use AppBundle\Entity\Message;
use AppBundle\Form\RegistrationType;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Http\Firewall\DigestAuthenticationListener;


class RegistrationController extends Controller
{


    private function authenticateUser(User $user)
    {
        $providerKey = 'main';
        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());

        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
    }


    public function registerAction(Request $request, \Swift_Mailer $mailer)
    {
        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($user->getParent() == null) {
                $form->get('parent')->addError(new FormError("Партнер не определен"));
            } else {

                $messagePass = $user->getPassword();
                $password = $this
                    ->get('security.password_encoder')
                    ->encodePassword($user, $user->getPassword());
                $user->setPassword($password);

                $em = $this->getDoctrine()->getManager();
                $user->setRoles(['ROLE_GUEST'])
                    ->setPhotoPath('av-zagl.png');
                $user->setRegistratedAt(new \DateTime('now'));

                $em->persist($user);

                $this->authenticateUser($user);

                $timezone = new \DateTimeZone('Europe/Moscow');
                $date = new \DateTime('now', $timezone);

                $support = $this->getDoctrine()->getRepository('AppBundle:User')->findOneByEmail('support@admin.ru');
                $dialog = new Dialog();
                $dialog->setFrom($support)->setTo($user)->setLastActive($date);
                $message = new Message();
                $message->setTo($user)->setFrom($support)->setDialog($dialog)->setDate($dialog->getLastActive())->setText('Добрый день! Служба технической поддержки команды VDOCTATKE - к вашим услугам!');
                $dialog->addMessage($message);
                $em->persist($dialog);
                $em->persist($message);

                $parent = $user->getParent();
                $dialog = new Dialog();
                $dialog->setFrom($parent)->setTo($user)->setLastActive($date);
                $message = new Message();
                $message->setTo($user)->setFrom($parent)->setDialog($dialog)->setDate($dialog->getLastActive())->setText('Добрый день! Я ваш информационный куратор, будут вопросы - обращайтесь!');
                $dialog->addMessage($message);
                $em->persist($dialog);
                $em->persist($message);

                $em->flush();

                //send message with password
                $message = (new \Swift_Message('Добро пожаловать на командный ресурс VD!'))
                    ->setFrom('nmbakhtiyarov@gmail.com')
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->renderView(
                            'email/mail.html.twig', [
                                'uname' => $user->getFirstName(),
                                'pass' => $messagePass,
                                'login' => $user->getEmail()
                            ]
                        ),
                        'text/html'
                    );
                $mailer->send($message);

                return $this->redirectToRoute('step_guest', ["number" => 0]);
            }
        }

        $view = $form->createView();
        if ($request->get("u") != null && empty($view->children["parent"]->vars["value"])) {
            $view->children["parent"]->vars["value"] = $request->get("u");
            $view->children["parent"]->vars["attr"]["readonly"] = true;
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            return $this->render('login.html.twig', array(
                'registerForm' => $view,
                '_fragment' => 'reg_error',
                'inValidRegForm' => true
            ));
        }

        return $this->render('login.html.twig', array(
            'registerForm' => $view,
        ));
    }

}