<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 11.07.17
 * Time: 3:33
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Dialog;
use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use AppBundle\Repository\DialogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class SupportMessagesController
 * @package AppBundle\Controller
 */
class SupportMessagesController extends Controller
{
    /**
     * @Route("/cabinet/supmessages", name="supdialogs-list")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showDialogs(Request $request)
    {
        //диалоги создаются от родителя к дочери
        $em = $this->getDoctrine()->getManager();

        $dialogRepository = $em->getRepository('AppBundle:Dialog');
        /**
         * @var User $user
         */
        $user = $em->getRepository('AppBundle:User')->findOneByEmail('support@admin.ru');

            $supportDialogs = $dialogRepository->getDialogsForSupport($user);
            if ($request->isXmlHttpRequest()) {
                return $this->render(':pages/messagesForAdminFromSupport:dialogs.html.twig', [
                    'partnerDialogs' =>  $supportDialogs,
                    'suser'=>$user
                ]);
            }
            return $this->render(':pages/messagesForAdminFromSupport:generalList.html.twig', [
                'partnerDialogs' => $supportDialogs,
                'suser'=>$user
            ]);
    }

    /**
     * @Route("/cabinet/supmessages/show", name="supmessages-list")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showMessages(Request $request, EntityManagerInterface $em)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->findOneByEmail('support@admin.ru');
        $requestDataId = $request->get('id');
        $dialogsRepository = $em->getRepository('AppBundle:Dialog');

        $messages = $dialogsRepository->findOneById($requestDataId);
        if ($messages != null) {
            foreach ($messages->getMessages() as $mes) {
                if ($mes->getFrom() != $user && !$mes->getIsReaded()) {
                    $mes->setIsReaded(true);
                    $em->persist($mes);
                }
            }
            $em->flush();
        }
        return $this->render(':pages/messagesForAdminFromSupport:mesages.html.twig', [
            'dialog' => $messages,
            'suser'=>$user
        ]);
    }

    /**
     * @Route("/cabinet/supmessages/create", name="supmessage-create")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createMessage(Request $request, EntityManagerInterface $em)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->findOneByEmail('support@admin.ru');
        $dialogsRepository = $em->getRepository('AppBundle:Dialog');

        $timezone = new \DateTimeZone('Europe/Moscow');
        $date = new \DateTime('now', $timezone);

        $id = $request->get('id');
        $text = $request->get('text');
        if (is_numeric($id)) {
            /**
             * @var Dialog $dialog
             */
            $dialog = $dialogsRepository->findOneById($id);
            $message = new Message();
            $message->setDate($date)->setDialog($dialog)->setFrom($user)->setText($text);
            $message->setTo($dialog->getFrom());
            if ($dialog->getFrom() == $user) {
                $message->setTo($dialog->getTo());
            }

            $em->persist($message);
            $dialog->addMessage($message)->setLastActive($message->getDate());
            $em->persist($dialog);
            $em->flush();

        } else {
            $id = substr($id, 3);
            $userRepository = $em->getRepository('AppBundle:User');
            $user2 = $userRepository->findOneById($id);
            $dialog = new Dialog();
            $from = $user2;
            $to = $user;
            if ($user->getId() < $user2->getId()) {
                $from = $user;
                $to = $user2;
            }
            $dialog->setFrom($from)->setTo($to)->setLastActive($date);
            $message = new Message();
            $message->setDate($dialog->getLastActive())->setDialog($dialog)->setFrom($user)->setText($text);
            $message->setTo($dialog->getFrom());
            if ($dialog->getFrom() == $user) {
                $message->setTo($dialog->getTo());
            }
            $em->persist($message);
            $dialog->addMessage($message);
            $em->persist($dialog);
            $em->flush();
            $dialog = $dialogsRepository->findOneBy(['from' => $from, 'to' => $to]);

        }
        return $this->render(':pages/messagesForAdminFromSupport:mesages.html.twig', [
            'dialog' => $dialog,
            'suser'=>$user
        ]);
    }

}