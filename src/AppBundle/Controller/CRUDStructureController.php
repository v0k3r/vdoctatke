<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 01.10.17
 * Time: 4:53
 */

namespace AppBundle\Controller;

use AppBundle\DTO\CreateFolder;
use AppBundle\Entity\ArchiveFile;
use AppBundle\Entity\User;
use AppBundle\Form\CreateFolderType;
use AppBundle\Repository\UserRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CoreController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
class CRUDStructureController extends CoreController
{

    public function listAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            return $this->tree();
        }
        $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
        $user = $userRepository->findOneBy([]);
        $calculatedStat=$this->calcStructureStat($user);
        $criteria = Criteria::create()->andWhere(Criteria::expr()->orX(
            Criteria::expr()->contains('roles', 'ROLE_PARTNER'),
            Criteria::expr()->contains('roles', 'ROLE_ADMIN')
        ));
        return $this->render('CRUD/structure-tree.html.twig', array(
            'base_template' => $this->getBaseTemplate(),
            'admin_pool' => $this->container->get('sonata.admin.pool'),
            'blocks' => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'),
            'user' => [
                'id' => $user->getId(),
                'name' => $user->getFirstName() . " " . $user->getLastName(),
                'phone' => $user->getTel(),
                'idso' => $this->getIntFromStr($user->getSureoneId()),
                'vk' => $user->getVk(),
                'whatsapp' => $user->getWhatsup(),
                'telegram' => $user->getTelegram(),
                'personalCount' => $userRepository->countPersonalRefs($user),
                'structCount' => count($userRepository->getChildEntities([$user->getId()],  array(), $criteria)),
                'stat' => $this->calcStats($user),
                'photo' => $user->getPhotoPath(),
                'roles' => $user->getRoles(),
                'lan'=>$user->getLandingName(),


            ],
            'structure'=>$calculatedStat,
            'fullStructure'=>$calculatedStat,

        ));

    }
    private function tree()
    {

        /** @var UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
        /** @var User $me */
        $me = $userRepository->findOneBy([]);
        $criteria = Criteria::create()->andWhere(Criteria::expr()->orX(
            Criteria::expr()->contains('roles', 'ROLE_PARTNER'),
            Criteria::expr()->contains('roles', 'ROLE_ADMIN'),
            Criteria::expr()->contains('roles', 'ROLE_BANNED')
        ));
        $users = $userRepository->getChildEntities($me, array(), $criteria);
        $rows = [];
        $rows[] = [
            'id' => '' . $me->getId(),
            'parent' => '#',
            'text' => $me->getFirstName() . ' ' . $me->getLastName(),
            'icon' => (!$me->getPhotoPath()) ? '/img/av-zagl.png' : '/photos' . '/' . $me->getPhotoPath()
        ];
        /** @var User $user */
        foreach ($users as $user) {
            $rows[] = [
                'id' => '' . $user->getId(),
                'parent' => '' . $user->getParent()->getId(),
                'text' => $user->getFirstName() . ' ' . $user->getLastName() . ' (' . $user->getCity(). ')',
                'icon' => (!$user->getPhotoPath()) ? '/img/av-zagl.png' : '/photos' . '/' . $user->getPhotoPath()
            ];
        }
        return new JsonResponse($rows);

    }
    private function getIntFromStr($str){
        if ($str!=null) {
            preg_match_all('|\d+|', $str, $matches);

            return $matches[0][0];
        }
        else return "Неопределен";
    }
    private function calcStats(User $user)
    {
        $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
        return [
            'views' => $user->getViews(),
            'leftData' => $userRepository->countReferalsLeftData($user),
            'interested' => $userRepository->countInterestedRefs($user),
            'payed' => $userRepository->countPayed($user),
            'other' => $userRepository->countOtherRef($user),
        ];
    }
    /**
     * @Route("/admin/structure", name="admin-structure")
     */
    public function structurePage(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            return $this->tree();
        }
        $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
        $user = $userRepository->findOneBy([]);
        $criteria = Criteria::create()->andWhere(Criteria::expr()->orX(
            Criteria::expr()->contains('roles', 'ROLE_PARTNER'),
            Criteria::expr()->contains('roles', 'ROLE_ADMIN')
        ));
        $calculatedStat=$this->calcStructureStat($user);
        return $this->render('CRUD/structure-tree.html.twig', [
            'user' => [
                'id' => $user->getId(),
                'name' => $user->getFirstName() . " " . $user->getLastName(),
                'phone' => $user->getTel(),
                'idso' => $this->getIntFromStr($user->getSureoneId()),
                'vk' => $user->getVk(),
                'whatsapp' => $user->getWhatsup(),
                'telegram' => $user->getTelegram(),
                'personalCount' => $userRepository->countPersonalRefs($user),
                'structCount' => count($userRepository->getChildEntities([$user->getId()],  array(), $criteria)),
                'stat' => $this->calcStats($user),
                'photo' => $user->getPhotoPath(),
                'roles' => $user->getRoles(),
                'lan'=>$user->getLandingName(),


            ],
            'structure'=>$calculatedStat,
            'fullStructure'=>$calculatedStat,
        ]);
    }
    /**
     * @Route("/admin/getuser/{id}")
     * @param Request $request
     * @param $id
     * @param EntityManagerInterface $entityManager
     * @return string
     */
    public function getUserId(Request $request, $id, EntityManagerInterface $entityManager)
    {
        $userRepository = $entityManager->getRepository('AppBundle:User');
        /** @var User $user */
        $user = $userRepository->find($id);
        $rootUser=$userRepository->findOneBy([]);
        $criteria = Criteria::create()->andWhere(Criteria::expr()->orX(
            Criteria::expr()->contains('roles', 'ROLE_PARTNER'),
            Criteria::expr()->contains('roles', 'ROLE_ADMIN')
        ));
        $view = $this->renderView('CRUD/structure.info.html.twig', [
            'user' => [
                'id' => $user->getId(),
                'name' => $user->getFirstName() . " " . $user->getLastName(). " (" . $user->getCity(). ")",
                'phone' => $user->getTel(),
                'idso' => $this->getIntFromStr($user->getSureoneId()),
                'vk' => $user->getVk(),
                'whatsapp' => $user->getWhatsup(),
                'telegram' => $user->getTelegram(),
                'personalCount' => $userRepository->countPersonalRefs($user),
                'structCount' => count($userRepository->getChildEntities([$user->getId()],  array(), $criteria)),
                'stat' => $this->calcStats($user),
                'photo' => $user->getPhotoPath(),
                'roles' => $user->getRoles(),
                'lan'=>$user->getLandingName(),
            ],
            'structure'=>$this->calcStructureStat($user),
            'fullStructure'=>$this->calcStructureStat($rootUser)
        ]);
        return new JsonResponse([
            'view' => $view
        ]);
    }
    private function calcStructureStat($user)
    {
        $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->orX(
                Criteria::expr()->contains('roles', 'ROLE_PARTNER'),
                Criteria::expr()->contains('roles', 'ROLE_ADMIN')
            ));
        $childs = $userRepository->getChildEntities($user, [], $criteria);
        $res = [];
        foreach ($childs as $child) {
            $res[] = $this->calcStats($child);
        }
        $res[] = $this->calcStats($user);
        $final = array();

        array_walk_recursive($res, function($item, $key) use (&$final){
            $final[$key] = isset($final[$key]) ?  $item + $final[$key] : $item;
        });
        return $final;
    }
}