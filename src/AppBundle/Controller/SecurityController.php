<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 07.03.17
 * Time: 10:55
 */

namespace AppBundle\Controller;


use AppBundle\Entity\MainVideo;
use AppBundle\Form\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use AppBundle\Entity\User;

/**
 * Class SecurityController
 * @package AppBundle\Controller
 */
class SecurityController extends Controller
{
    /**
     * @Route("/auth/login", name="login")
     */
    public function loginAction(Request $request,EntityManagerInterface $entityManager)
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $view=$this->createForm(RegistrationType::class, new User())->createView();
        if ($request->get("u")!=null&&empty($view->children["parent"]->vars["value"])){
            $view->children["parent"]->vars["value"]=$request->get("u");
            $view->children["parent"]->vars["disabled"]=true;
        }

        if ($error!=null)
            return $this->render('login.html.twig', array(
                'inValidLoginForm' => $error,
                '_fragment' => 'log_error',
                'registerForm' => $this->createForm(RegistrationType::class, new User())->createView(),

            ));
        return $this->render('login.html.twig', array(
            'inValidLoginForm' => $error,
            'registerForm' => $view,
        ));

    }

    /**
     * @Route("/auth/dispatch")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function dispatch(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $role = $user->getRoles();
        $em=$this->getDoctrine()->getManager();
        /**
         * @var MainVideo $video
         */
        $video = $em->getRepository('AppBundle:MainVideo')->findOneByType('training');


        if (in_array('ROLE_NEW_PARTNER', $role)) {
            return $this->redirectToRoute('cabinet_create-landing');
        } elseif (in_array('ROLE_PARTNER', $role) || in_array('ROLE_ADMIN', $role)) {
            if ($user->getLastTrainingVisit()==null || $video->getDate() > $user->getLastTrainingVisit()) {
                return $this->redirectToRoute('cabinet_training');
            }
            return $this->redirectToRoute('cabinet_client');
        } elseif (in_array('ROLE_GUEST', $role)) {
            return $this->redirectToRoute('step_guest', ['number' => $user->getStep()]);
        } elseif (in_array('ROLE_USER', $role)) {
            return $this->redirectToRoute('cabinet_wait');
        }
        return new RedirectResponse('/');
    }
}