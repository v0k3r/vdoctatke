<?php
/**
 * IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH 
 * 
 * The following source code is PROPRIETARY AND CONFIDENTIAL. Use of this source code 
 * is governed by the IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. Non-Disclosure Agreement 
 * previously entered between you and IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. 
 * 
 * By accessing, using, copying, modifying or distributing this software, you acknowledge 
 * that you have been informed of your obligations under the Agreement and agree 
 * to abide by those obligations. 
 * 
 * @author vladislav <vladislav.baymurzin@i-a-t.net>  
 */


namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MailController extends Controller
{

    /**
     * @Route("/cabinet/shop/sendreqs", name="send_requisites")
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sendRequisites(Request $request, \Swift_Mailer $mailer)
    {
        $user = $this->getUser();
        $message = (new \Swift_Message('Реквизиты для оплаты VDOCTATKE!'))
            ->setFrom('nmbakhtiyarov@gmail.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    'email/buycourse.reqs.html.twig',[
                        'uname'=>$user->getFirstName()
                    ]
                ),
                'text/html'
            )
        ;
        $mailer->send($message);
        return $this->render('pages/shop.html.twig', [
            'statusResponse' => 1
        ]);
    }
}