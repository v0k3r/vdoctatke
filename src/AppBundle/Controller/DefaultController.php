<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function registerAction(Request $request,EntityManagerInterface $entityManager)
    {
        $dates = $entityManager->getRepository('AppBundle:LandingEvent')->getFutureOnedayDate();
        $oneDate = null;
        $pDate = null;
        if ($dates != null) {
            $oneDate = $dates[0];
        }
        $dates = $entityManager->getRepository('AppBundle:LandingEvent')->getFuturePeriodicDate();
        if ($dates != null) {
            $pDate = $dates[0];
        } else {
            $pDate = $entityManager->getRepository('AppBundle:LandingEvent')->findBy(['type' => 'Периодическое'], ['weekNumber' => 'ASC']);
            if ($pDate!=null) {
                $pDate = $pDate[0];
            }
        }

        if ($pDate == null) {
            if ($oneDate == null) {
                $date = 'Скоро анонс';
                $time = 'время по';
            } else {
                $date = $oneDate->getDatetime();
                $time = $oneDate->getTime();
            }
        } elseif ($oneDate != null) {

            $d = $oneDate->getDatetime();
            if ($d->format('w') <= $pDate->getWeekNumber()) {
                $date = $oneDate->getDatetime();
                $time = $oneDate->getTime();
            } else {
                $time = $pDate->getTime();
                $date = new \DateTime('now');
                while ($date->format('w') != $pDate->getWeekNumber()) {
                    $date->modify('+1 day');
                }

            }
        } elseif ($oneDate == null) {
            $time = $pDate->getTime();
            $date = new \DateTime('now');
            while ($date->format('w') != $pDate->getWeekNumber()) {
                $date->modify('+1 day');
            }
        }


        if ($pDate != null || $oneDate != null) {
            $formatter = new \IntlDateFormatter('ru_RU', \IntlDateFormatter::FULL, \IntlDateFormatter::FULL);
            $formatter->setPattern('d MMMM y');
            $date = $formatter->format($date);
        }
        $recommendations=$entityManager->getRepository('AppBundle:Recommendation')->findAll();
        // replace this example code with whatever you need
        return $this->render('main.html.twig',
            [  'recommendations'=>$recommendations,
                'date' => $date,
                'time' => $time,
                'error' =>'',
                'last_username'=>' '
            ]);
    }

}
