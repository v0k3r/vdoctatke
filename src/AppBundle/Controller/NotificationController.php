<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 28.07.17
 * Time: 23:11
 */

namespace AppBundle\Controller;


use AppBundle\Entity\MainVideo;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class NotificationController extends Controller
{
    /**
     * @Route("/cabinet/notification", name="notification")
     */
    public function requestAction()
    {
        $array = array(0, 0, 0,0,0);
        return new JsonResponse($array);
    }

    public function getUnreadMessagesCountAction()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $count = $em->getRepository('AppBundle:Message')->getUnreadedCount($user);
        return new Response($count);
    }

    public function getUnwatchedSupportMessagesAction()
    {
        $em=$this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->findOneByEmail('support@admin.ru');
        $em = $this->getDoctrine()->getManager();
        $count = $em->getRepository('AppBundle:Message')->getUnreadedCount($user);
        return new Response($count);
    }

    public function getUnreadNewsCountAction()
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        if ($user->getLastNewsVisit() == null) {
            $count = $em->getRepository('AppBundle:News')->getAllNewsCount();
        } else {
            $count = $em->getRepository('AppBundle:News')->getUnreadedNewsCount($this->getUser());
        }
        if ($count==0)
            $count=" ";
        else $count="($count)";
        return new Response($count);
    }


    public function getUnwatchedRequestsCountAction()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $count = count($em->getRepository('AppBundle:User')->getUserRequests($user));
        return new Response($count);
    }
    /**
     * @Route("/cabinet/requests/count", name="request-notification-count")
     */
    public function getNewRequestsCountAction()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $count = count($em->getRepository('AppBundle:User')->getUserRequests($user));
        return new Response($count);
    }
    public function getUnwatchedTrainingAction()
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        if ($user->getLastTrainingVisit() == null) {
            $count = 1;
        } else {
            /**
             * @var MainVideo $video
             */
            $video = $em->getRepository('AppBundle:MainVideo')->findOneByType('training');
            if ($video->getDate() > $user->getLastTrainingVisit()) {
                $count = 1;
            } else {
                $count = 0;
            }
        }
        return new Response($count);
    }
}