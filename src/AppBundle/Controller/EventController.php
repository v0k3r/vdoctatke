<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Dialog;
use AppBundle\Entity\LandingEvent;
use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use AppBundle\Form\PartialRegistrationType;
use AppBundle\Form\RegistrationType;

use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Constraints\Date;

class EventController extends Controller
{
    /**
     * @Route("/cabinet/event",name="cabinet-event")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function eventList(Request $request)
    {
        if ($request->isXmlHttpRequest()) {

            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());

            $serializer = new Serializer($normalizers, $encoders);

            $em = $this->getDoctrine()->getManager();
            $events = $em->getRepository('AppBundle:LandingEvent')->findAll();
            $output = $jsonContent = $serializer->serialize($events, 'json');
            return new Response($output);
        }
        return $this->render('pages/event/list.html.twig', [

        ]);

    }

    /**
     * @Route("/event/delete",name="event-delete")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function eventDelete(Request $request)
    {
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $deleteEvent = $em->getRepository('AppBundle:LandingEvent')->findOneById($id);
        $em->remove($deleteEvent);
        $em->flush();
        return new Response();
    }

    /**
     * @Route("/event/create",name="event-create")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function eventCreate(Request $request)
    {
        /*
         * Одиночное, периодическое - 2 типа
         */
        $em = $this->getDoctrine()->getManager();
        $event = new LandingEvent();
        $type = $request->request->get('type');
        $data = trim($request->request->get('data'));
        if (strcasecmp('oneday', $type) == 0) {
            $str = str_replace(" ", "", $data);
            $datetime = new \DateTime(substr($str, 0, 10));
            $time = substr($data, 10, 6);
            $event->setDatetime($datetime)->setTime($time)->setType('Одиночное');
            $em->persist($event);
            $em->flush();
        } else if (strcasecmp('everyweek', $type) == 0) {
           $time=$request->request->get('time');
           $event->setType('Периодическое')->setWeekNumber($data)->setTime($time);
            $em->persist($event);
            $em->flush();
        }

        return new Response();
    }

}
