<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 01.10.17
 * Time: 4:53
 */

namespace AppBundle\Controller;

use AppBundle\DTO\CreateFolder;
use AppBundle\Entity\ArchiveFile;
use AppBundle\Entity\User;
use AppBundle\Form\CreateFolderType;
use AppBundle\Repository\UserRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CoreController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
class CRUDEventController extends CoreController
{

    public function listAction(Request $request)
    {

    return $this->render('CRUD/list.html.twig', array(
        'base_template' => $this->getBaseTemplate(),
        'admin_pool' => $this->container->get('sonata.admin.pool'),
        'blocks' => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'),


    ));
    }
    public function createAction(Request $request){
            return $this->render('CRUD/create.html.twig');
    }


}