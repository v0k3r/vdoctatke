<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 24.08.17
 * Time: 20:27
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ResetPasswordController extends Controller
{
    private function authenticateUser(User $user)
    {
        $providerKey = 'main';
        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());

        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
    }
    /**
     * @Route("/reset/password", name="resetpass")
     */
    public function indexAction(Request $request,\Swift_Mailer $mailer)
    {
     $email=$request->request->get('email');
     $errors=null;
        if($email!=''){
         $em = $this->getDoctrine()->getRepository('AppBundle:User');
            /**
             * @var User $foundedUser
             */
         $foundedUser = $em->findOneByEmail($email);
         if ($foundedUser == null) {
             $errors = "Пользователя с таким email не существует.";
         } else {
             $hashKey=md5($foundedUser->getId()).md5($foundedUser->getEmail()).md5(substr($foundedUser->getPassword(),-3));

             $url="vdoctatke.pro/changepass/".$foundedUser->getId().'/'.$hashKey;
             $vk_api_url = "https://api.vk.com/method/utils.getShortLink?url=$url&private=0&access_token=cc53fb1bcc53fb1bcc53fb1b1ecc0c493accc53cc53fb1b96443444d2071d20e44a0b3e&v=5.92";
             try {
                 $ch = curl_init();
                 curl_setopt($ch, CURLOPT_URL, $vk_api_url);
                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                 $output = curl_exec($ch);
                 curl_close($ch);
                 $json = json_decode($output, true);
                 $lnk = ($json["response"]["short_url"]);

                 $message = (new \Swift_Message('Запрос на смену пароля'))
                     ->setFrom('nmbakhtiyarov@gmail.com')
                     ->setTo($foundedUser->getEmail())
                     ->setBody(
                         $this->renderView(
                             'email/resetPass.html.twig', [
                                 'lnk' => $lnk,
                                 'uname' => $foundedUser->getFirstName(),
                                 'login' => $foundedUser->getEmail()
                             ]
                         ),
                         'text/html'
                     );
                 $mailer->send($message);
                 $errors = 'Вам на почту отправлена инструкция сброса пароля.';

             }
             catch (\Exception $e){
                 $errors = 'Произошла ошибка. Обратитесь к администратору';

             }


         }
     }
     return $this->render('pages/resetPass/send.html.twig',[
         'errors'=>$errors
     ]);

    }
    /**
     * @Route("/changepass/{id}/{hash}", name="changepass")
     */
    public function changePasswordAction($id,$hash,Request $request,\Swift_Mailer $mailer){
        $errors=null;
        $em = $this->getDoctrine()->getRepository('AppBundle:User');
        $foundedUser = $em->findOneById($id);
        $form = $this->createFormBuilder()
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password'),
            ))
            ->getForm();
        if ($foundedUser == null) {
            $errors = "Пользователя с таким email не существует.";
        }
        else{
            $checkHash=md5($foundedUser->getId()).md5($foundedUser->getEmail()).md5(substr($foundedUser->getPassword(),-3));
            if (strcasecmp($checkHash,$hash)==0){

                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()){
                     $messagePass=$form->get('password')->getData();
                    $password = $this
                        ->get('security.password_encoder')
                        ->encodePassword($foundedUser, $form->get('password')->getData());
                    $foundedUser->setPassword($password);
                    $em=$this->getDoctrine()->getManager();
                    $em->persist($foundedUser);
                    $em->flush();
                 $this->authenticateUser($foundedUser);

                    $message = (new \Swift_Message('Пароль успешно сброшен!'))
                        ->setFrom('nmbakhtiyarov@gmail.com')
                        ->setTo($foundedUser->getEmail())
                        ->setBody(
                            $this->renderView(
                                'email/succesmail.html.twig',[
                                    'uname'=>$foundedUser->getFirstName(),
                                    'pass'=>$messagePass,
                                    'login'=>$foundedUser->getEmail()
                                ]
                            ),
                            'text/html'
                        )
                    ;
                    $mailer->send($message);


                    $role = $foundedUser->getRoles();
                    if (in_array('ROLE_NEW_PARTNER', $role)) {
                        return $this->redirectToRoute('cabinet_create-landing');
                    } elseif (in_array('ROLE_PARTNER', $role) || in_array('ROLE_ADMIN', $role)) {
                        return $this->redirectToRoute('cabinet_client');
                    } elseif (in_array('ROLE_GUEST', $role)) {
                        return $this->redirectToRoute('step_guest', ['number' => 7]);
                    } elseif (in_array('ROLE_USER', $role)) {
                        return $this->redirectToRoute('cabinet_wait');
                    }
                    return new RedirectResponse('/');

                }
            }
            else{
                $errors="Неверная ссылка либо срок ссылки истек. Внимательно проверьте ссылку.";
            }
        }
        return $this->render('pages/resetPass/changeNewPass.html.twig',[
            'errors'=>$errors,
            'form'=>$form->createView()
        ]);
    }
}