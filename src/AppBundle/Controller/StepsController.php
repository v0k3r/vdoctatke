<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 02.07.17
 * Time: 18:20
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\FullStepsRegistrationType;
use AppBundle\Form\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;


class StepsController extends Controller
{

    private function authenticateUser(User $user)
    {
        $providerKey = 'main';
        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());

        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
    }

    /**
     * @Route("/steps/{number}", name="step_guest")
     * @param int $number
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($number, Request $request, EntityManagerInterface $em,\Swift_Mailer $mailer)
    {



        $form = $this->createFormBuilder()
            ->add('ok', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        /**
         * @var User $user
         **/
        $user = $this->getUser();

            $isTrue = $form->get('ok')->isClicked();
            if ($number > 10 || $number == null || !is_numeric($number)) {
                $number = $user->getStep();
            }
           else{
               $user->setStep($number);
               $em->persist($user);
               $em->flush();
           }
            if ($isTrue) {
                if ($number == $user->getStep()+1) {
                    $user->setStep($number + 1);
                    $em->persist($user);
                    $em->flush();

                }
                $number++;
                $number = (!$number) ? 1 : $number;
                return $this->redirect("/steps/$number");
            }


        $formReg = $this->createForm(FullStepsRegistrationType::class, $user);
        $formReg->handleRequest($request);
        if ($formReg->isSubmitted() && $formReg->isValid()) {

            if ($user->getVk() == null ||  $user->getVk()=='http://vk.com/' || $user->getLastName() == null || $user->getTelegram() == null || $user->getTel() == null ||
                $user->getWhatsup() == null || $user->getCity() == null || $user->getCountry() == null || $user->getEmail() == null || $user->getSureoneId() == null || !$this->getIntFromStr($user->getSureoneId())
            ) {
                if($user->getVk() == null ||  $user->getVk()=='http://vk.com/'){
                    $formReg->get('first_name')->addError(new FormError('Поле vk.com не заполнено'));
                }
                $formReg->get('first_name')->addError(new FormError('Поля не должны быть пустыми и должны соответсвовать реальным данным'));
            } else {

                $user->setRoles(['ROLE_USER']);
                $messagePass=$user->getPassword();
                $password = $this
                    ->get('security.password_encoder')
                    ->encodePassword($user, $user->getPassword());
                $user->setPassword($password);
                $em->persist($user);
                $em->flush();
                $this->authenticateUser($user);
                try {
                    $message = (new \Swift_Message('Спасибо за регистрацию!'))
                        ->setFrom('nmbakhtiyarov@gmail.com')
                        ->setTo($user->getEmail())
                        ->setBody(
                            $this->renderView(
                                'email/mail.html.twig', [
                                    'uname' => $user->getFirstName(),
                                    'pass' => $messagePass,
                                    'login' => $user->getEmail()
                                ]
                            ),
                            'text/html'
                        );
                    $mailer->send($message);

                }
                catch (\Exception $err){

                }
                return $this->redirectToRoute('cabinet_wait');
            }
        }
        $number = $number ? $number : 1;
        if ($number>10){
            $number=$user->getStep();
            if ($number>10){
                $number=10;
            }
        }
        return $this->render("pages/steps/$number.html.twig", [
            "form" => $form->createView(),
            'url' => "/steps/$number",
            'formReg' => $formReg->createView()

        ]);
    }

    /**
     * @Route("/cabinet/stepsSucces")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAction(Request $request, EntityManagerInterface $em)
    {
        $session = new Session();

        $session->set('isWatched', 'true');
        return new JsonResponse();
    }
    private function getIntFromStr($str){
        if ($str!=null) {
            preg_match_all('|\d+|', $str, $matches);

            if ($matches[0]){
                if (strlen($matches[0][0])>=4) {
                    return true;
                }
                else return false;
            }
            else return false;
        }
        else return false;
    }
}