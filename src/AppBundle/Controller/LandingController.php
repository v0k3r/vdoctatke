<?php
/**
 * IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH 
 * 
 * The following source code is PROPRIETARY AND CONFIDENTIAL. Use of this source code 
 * is governed by the IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. Non-Disclosure Agreement 
 * previously entered between you and IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. 
 * 
 * By accessing, using, copying, modifying or distributing this software, you acknowledge 
 * that you have been informed of your obligations under the Agreement and agree 
 * to abide by those obligations. 
 * 
 * @author vladislav <vladislav.baymurzin@i-a-t.net>  
 */


namespace AppBundle\Controller;


use AppBundle\DTO\CreateLanding;
use AppBundle\Entity\Dialog;
use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use AppBundle\Form\CreateLandingType;
use AppBundle\Form\PartialRegistrationType;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class LandingController extends Controller
{
    private function authenticateUser(User $user)
    {
        $providerKey = 'main';
        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());

        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
    }

    /**
     * @Route("/cabinet/createLanding", name="cabinet_create-landing")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createLanding(Request $request, EntityManagerInterface $entityManager)
    {
        $error = null;
        $landingDTO = new CreateLanding();
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $formLanding = $this->createForm(CreateLandingType::class, $landingDTO);
        $formLanding->handleRequest($request);
        /** @var UserRepository $userRepository */
        $userRepository = $entityManager->getRepository('AppBundle:User');
        $ln = $userRepository->findOneByLandingName($landingDTO->username);
        $lnFlag=preg_match("/^[A-Za-z0-9-_]{3,15}$/",$landingDTO->username);
        if ($formLanding->isSubmitted() && $formLanding->isValid()) {
            if (!$lnFlag){
                $error.='Неверный формат лэндинга';
            }
            if ($ln == null&&$lnFlag) {
                /**
                 * @var $photo UploadedFile
                 */
                $photo = $landingDTO->photo;
                $savedPhoto = $userRepository->saveUserPhoto($photo, $currentUser, $this->get('kernel')->getRootDir());
                $fileName = $savedPhoto;
                $currentUser->setPhotoPath($fileName);
                $currentUser->setLandingName($landingDTO->username);
                if ($landingDTO->sureone != '') {
                    $currentUser->setSureoneId($landingDTO->sureone);
                } else {
                    $currentUser->setSureoneId($request->request->get('otherRef'));
                }
                $currentUser->setRoles(['ROLE_PARTNER']);
                $entityManager->flush();
                $token = $this->get('security.token_storage')->getToken();
                $token->setAuthenticated(false);
                $this->get('session')->migrate();
                return new RedirectResponse($this->generateUrl('cabinet_training'));
            } elseif($ln!=null) {
                $error .= 'Лэндинг уже занят';
            }

        }


        return $this->render('pages/cabinet/createLanding.html.twig', [
            'formLanding' => $formLanding->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/{username}", name="show_landing")
     * @param string $username
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showLanding($username, Request $request, EntityManagerInterface $entityManager, \Swift_Mailer $mailer)
    {
        /** @var UserRepository $userRepository */
        $userRepository = $entityManager->getRepository('AppBundle:User');
        /** @var User $user */
        $user = $userRepository->findOneByLandingName($username);
        if (is_null($user))
            throw $this->createNotFoundException('Not found user with name ' . $username);

        $newUser = new User();
        if (!in_array($request->getClientIp(), $user->getIp())) {
            $user->setViews($user->getViews() + 1);
            $user->setIp(array_merge($user->getIp(), [$request->getClientIp()]));
        }
        $entityManager->persist($user);
        $entityManager->flush();

//        datetime to nearest event

        $dates = $entityManager->getRepository('AppBundle:LandingEvent')->getFutureOnedayDate();
        $oneDate = null;
        $pDate = null;
        if ($dates != null) {
            $oneDate = $dates[0];
        }
        $dates = $entityManager->getRepository('AppBundle:LandingEvent')->getFuturePeriodicDate();
        if ($dates != null) {
            $pDate = $dates[0];
        } else {
            $pDate = $entityManager->getRepository('AppBundle:LandingEvent')->findBy(['type' => 'Периодическое'], ['weekNumber' => 'ASC']);
            if ($pDate!=null) {
                $pDate = $pDate[0];
            }
        }

        if ($pDate == null) {
            if ($oneDate == null) {
                $date = 'Скоро анонс';
                $time = 'время по';
            } else {
                $date = $oneDate->getDatetime();
                $time = $oneDate->getTime();
            }
        } elseif ($oneDate != null) {

            $d = $oneDate->getDatetime();
            if ($d->format('w') <= $pDate->getWeekNumber()) {
                $date = $oneDate->getDatetime();
                $time = $oneDate->getTime();
            } else {
                $time = $pDate->getTime();
                $date = new \DateTime('now');
                while ($date->format('w') != $pDate->getWeekNumber()) {
                    $date->modify('+1 day');
                }

            }
        } elseif ($oneDate == null) {
            $time = $pDate->getTime();
            $date = new \DateTime('now');
            while ($date->format('w') != $pDate->getWeekNumber()) {
                $date->modify('+1 day');
            }
        }


        if ($pDate != null || $oneDate != null) {
            $formatter = new \IntlDateFormatter('ru_RU', \IntlDateFormatter::FULL, \IntlDateFormatter::FULL);
            $formatter->setPattern('d MMMM y');
            $date = $formatter->format($date);
        }

        $form = $this->createForm(PartialRegistrationType::class, $newUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($newUser->getFirstName() == null || $newUser->getEmail() == null || $newUser->getPassword() == null) {
                $form->get('first_name')->addError(new FormError('Поля не должны быть пустыми'));
            } else {
                $messagePass = $newUser->getPassword();
                $password = $this
                    ->get('security.password_encoder')
                    ->encodePassword($user, $newUser->getPassword());
                $newUser->setPassword($password)
                    ->setRoles(['ROLE_GUEST'])
                    ->setParent($user)
                     ->setPhotoPath('av-zagl.png');


                $timezone = new \DateTimeZone('Europe/Moscow');
                $date = new \DateTime('now', $timezone);
                $newUser->setRegistratedAt($date);
                $entityManager->persist($newUser);

                $support = $this->getDoctrine()->getRepository('AppBundle:User')->findOneByEmail('support@admin.ru');
                $dialog = new Dialog();
                $dialog->setFrom($support)->setTo($newUser)->setLastActive($date);
                $message = new Message();
                $message->setTo($newUser)->setFrom($support)->setDialog($dialog)->setDate($dialog->getLastActive())->setText('Добрый день! Служба технической поддержки команды VDOCTATKE - к вашим услугам!');
                $dialog->addMessage($message);
                $entityManager->persist($dialog);
                $entityManager->persist($message);

                $parent = $newUser->getParent();
                $dialog = new Dialog();
                $dialog->setFrom($parent)->setTo($newUser)->setLastActive($date);
                $message = new Message();
                $message->setTo($newUser)->setFrom($parent)->setDialog($dialog)->setDate($dialog->getLastActive())->setText('Добрый день! Я ваш информационный куратор, будут вопросы - обращайтесь!');
                $dialog->addMessage($message);
                $entityManager->persist($dialog);
                $entityManager->persist($message);

                $entityManager->flush();

                $message = (new \Swift_Message('Спасибо за регистрацию!'))
                    ->setFrom('nmbakhtiyarov@gmail.com')
                    ->setTo($newUser->getEmail())
                    ->setBody(
                        $this->renderView(
                            'email/mail.html.twig', [
                                'uname' => $newUser->getFirstName(),
                                'pass' => $messagePass,
                                'login' => $newUser->getEmail()
                            ]
                        ),
                        'text/html'
                    );
                $mailer->send($message);

                $this->authenticateUser($newUser);
                return $this->redirect("/steps/1");
            }
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            return $this->render('pages/cabinet/landing.html.twig', array(
                'date' => $date,
                'partRegForm' => $form->createView(),
                '_fragment' => 'partReg_error',
                'inValidPartRegForm' => true,
                'user' => $user,
                'time' => $time
            ));
        }
        $recommendations=$entityManager->getRepository('AppBundle:Recommendation')->findAll();
        $video=$entityManager->getRepository('AppBundle:MainVideo')->findOneByType('landing');
        return $this->render('pages/cabinet/landing.html.twig', [
            'user' => $user,
            'partRegForm' => $form->createView(),
            'date' => $date,
            'time' => $time,
            'recommendations'=>$recommendations,
            'video'=>$video
        ]);

    }

}