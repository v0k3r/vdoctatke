<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 11.07.17
 * Time: 3:33
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Dialog;
use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use AppBundle\Repository\DialogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class MessagesController
 * @package AppBundle\Controller
 */
class MessagesController extends Controller
{
    /**
     * @Route("/cabinet/messages", name="dialogs-list")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showDialogs(Request $request)
    {
        return $this->render("pages/dialog/moderation.html.twig");
        //диалоги создаются от родителя к дочери
        $em = $this->getDoctrine()->getManager();
        /**
         * @var User $user
         */
        $user = $this->getUser();
        $dialogRepository = $em->getRepository('AppBundle:Dialog');


        $parent = $user->getParent();
        $role = $user->getRoles();
        $support = $this->getDoctrine()->getRepository('AppBundle:User')->findOneByEmail('support@admin.ru');
        $supportDialog = $dialogRepository->findOneBy(['from' => $support, 'to' => $user]);
        if ($supportDialog == null) {
            $supportDialog = $dialogRepository->findOneBy(['to' => $support, 'from' => $user]);
            if ($supportDialog == null)
                $supportDialog = $support;
        }

        if ($user->getEmail() == 'support@admin.ru') {
            $supportDialogs = $dialogRepository->getDialogsForSupport($user);
            if ($request->isXmlHttpRequest()) {
                return $this->render(':pages/dialog:dialogs.html.twig', [
                    'supportDialog' => $supportDialog,
                    'parentDialogs' => $supportDialogs,
                    'unsubmitedDialogs' => null,
                    'partnerDialogs' => null,
                ]);
            }
            return $this->render(':pages/dialog:generalList.html.twig', [
                'supportDialog' => $supportDialog,
                'parentDialogs' => $supportDialogs,
                'unsubmitedDialogs' => null,
                'partnerDialogs' => null,


            ]);
        }

        $parrentDialogs = array();

        if ($parent != null) {
            for ($i = 0; $i < 5; $i++) {
                $dialog = $dialogRepository->findOneBy([
                    'from' => $parent,
                    'to' => $user
                ]);
                if ($dialog != null)
                    array_push($parrentDialogs, $dialog);
                else array_push($parrentDialogs, $parent);
                if ($parent->getParent() != null) {
                    $parent = $parent->getParent();
                } else break;
            }
        }
        $parrentDialogs = array_reverse($parrentDialogs);

        if (in_array('ROLE_NEW_PARTNER', $role) || in_array('ROLE_GUEST', $role) || in_array('ROLE_USER', $role)) {

            $dialogs = array();


            $others = $dialogRepository->getPartnerDialogs($user, $support);
            $dialogs = array_unique(array_merge($dialogs, $others));

            if ($request->isXmlHttpRequest()) {
                return $this->render(':pages/dialog:dialogs.html.twig', [
                    'supportDialog' => $supportDialog,
                    'parentDialogs' => $parrentDialogs,
                    'unsubmitedDialogs' => $dialogs,
                    'partnerDialogs' => null,
                ]);
            }
            return $this->render(':pages/dialog:generalList.html.twig', [
                'supportDialog' => $supportDialog,
                'parentDialogs' => $parrentDialogs,
                'unsubmitedDialogs' => $dialogs,
                'partnerDialogs' => null,


            ]);

        } elseif (in_array('ROLE_PARTNER', $role) || in_array('ROLE_ADMIN', $role)) {

            $userRepository = $em->getRepository('AppBundle:User');
            $partnerDialogs = $dialogRepository->getChildDialogs($user, $support);
            $partners = $userRepository->getChildEntitiesWithRole($user, $user);
            $partnerDialogs = array_merge($partnerDialogs, $partners);

            $unsubmitedChilds = $userRepository->getChildEntitiesWithNotRole($user, $user, 0);

            $unsubmitedDialogs = $dialogRepository->getUnsubmitedDialogs($user);
            $unsubmitedDialogs = array_merge($unsubmitedDialogs, $unsubmitedChilds);

            if ($request->isXmlHttpRequest()) {
                return $this->render(':pages/dialog:dialogs.html.twig', [
                    'supportDialog' => $supportDialog,
                    'parentDialogs' => $parrentDialogs,
                    'unsubmitedDialogs' => $unsubmitedDialogs,
                    'partnerDialogs' => $partnerDialogs,
                ]);
            }

            return $this->render(':pages/dialog:generalList.html.twig', [
                'supportDialog' => $supportDialog,
                'parentDialogs' => $parrentDialogs,
                'unsubmitedDialogs' => $unsubmitedDialogs,
                'partnerDialogs' => $partnerDialogs,
            ]);


        }
//        elseif (in_array('ROLE_ADMIN', $role)) {
//
//            $dialogs = $dialogRepository->getChildDialogs($user);
//            $userRepository = $em->getRepository('AppBundle:User');
//            $childs = $userRepository->getChildEntitiesWithNotRole($user, $user, -1);
//            $otherDialogs = $dialogRepository->getOtherDialogs($user, $support);
//            $others = $userRepository->getChildEntitiesWithRole($user, $user);
//            $otherDialogs = array_merge($otherDialogs, $others);
//            $dialogs = array_merge($dialogs, $childs);
//
//            if ($request->isXmlHttpRequest()) {
//                return $this->render('pages/message/messageDialogs.html.twig', [
//                    'partnerDialogs' => $dialogs,
//                    'otherDialogs' => $otherDialogs,
//                    'supportDialog' => $supportDialog
//                ]);
//            }
//            return $this->render('pages/message/messageGeneral.html.twig', [
//                'partnerDialogs' => $dialogs,
//                'otherDialogs' => $otherDialogs,
//                'supportDialog' => $supportDialog
//            ]);
//        }

        return $this->render('pages/message/messageGeneral.html.twig');
    }

    /**
     * @Route("/cabinet/messages/show", name="messages-list")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showMessages(Request $request, EntityManagerInterface $em)
    {

        $requestDataId = $request->get('id');
        $dialogsRepository = $em->getRepository('AppBundle:Dialog');

        $messages = $dialogsRepository->findOneById($requestDataId);
        if ($messages != null) {
            foreach ($messages->getMessages() as $mes) {
                if ($mes->getFrom() != $this->getUser() && !$mes->getIsReaded()) {
                    $mes->setIsReaded(true);
                    $em->persist($mes);
                }
            }
            $em->flush();
        }
        return $this->render(':pages/dialog:mesages.html.twig', [
            'dialog' => $messages,
        ]);
    }

    /**
     * @Route("/cabinet/messages/create", name="message-create")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createMessage(Request $request, EntityManagerInterface $em)
    {
        $dialogsRepository = $em->getRepository('AppBundle:Dialog');

        $timezone = new \DateTimeZone('Europe/Moscow');
        $date = new \DateTime('now', $timezone);

        $id = $request->get('id');
        $text = $request->get('text');
        if (is_numeric($id)) {
            /**
             * @var Dialog $dialog
             */
            $dialog = $dialogsRepository->findOneById($id);
            $message = new Message();
            $message->setDate($date)->setDialog($dialog)->setFrom($this->getUser())->setText($text);
            $message->setTo($dialog->getFrom());
            if ($dialog->getFrom() == $this->getUser()) {
                $message->setTo($dialog->getTo());
            }

            $em->persist($message);
            $dialog->addMessage($message)->setLastActive($message->getDate());
            $em->persist($dialog);
            $em->flush();

        } else {
            $id = substr($id, 3);
            $userRepository = $em->getRepository('AppBundle:User');
            $user2 = $userRepository->findOneById($id);
            $dialog = new Dialog();
            $from = $user2;
            $to = $this->getUser();
            if ($this->getUser()->getId() < $user2->getId()) {
                $from = $this->getUser();
                $to = $user2;
            }
            $dialog->setFrom($from)->setTo($to)->setLastActive($date);
            $message = new Message();
            $message->setDate($dialog->getLastActive())->setDialog($dialog)->setFrom($this->getUser())->setText($text);
            $message->setTo($dialog->getFrom());
            if ($dialog->getFrom() == $this->getUser()) {
                $message->setTo($dialog->getTo());
            }
            $em->persist($message);
            $dialog->addMessage($message);
            $em->persist($dialog);
            $em->flush();
            $dialog = $dialogsRepository->findOneBy(['from' => $from, 'to' => $to]);

        }
        return $this->render(':pages/dialog:mesages.html.twig', [
            'dialog' => $dialog
        ]);
    }

}