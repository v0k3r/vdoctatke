<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 22.06.17
 * Time: 17:55
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\ChangePasswordType;
use AppBundle\Form\ProfileType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SettingsController extends Controller
{
    /**
     * @Route("/cabinet/settings", name="settings")
     */
    public function waitAction(Request $request,\Swift_Mailer $mailer)
    {

        /**
         * @var User $user
         */
        $user = $this->getUser();
        $errors = null;
        $succes = null;
        $passSucces = null;
        $em = $this->getDoctrine()->getManager();
        $path = $user->getPhotoPath();//нужно, если у нас не залит файл аватарки, а $user обновляется напрямую из формы и будет null
        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($user->getVk() == null || $user->getTelegram() == null || $user->getTel() == null || $user->getWhatsup() == null) {
                $errors = "Поля должны быть заполнены и соответствовать реальным данным";
                $user->setUploadAvatar(null);

            } else {
                $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
                if ($user->getUploadAvatar() != null) {
                    $savedPhoto = $userRepository->saveUserPhoto($user->getUploadAvatar(), $user, $this->get('kernel')->getRootDir());
                    $fileName = $savedPhoto;
                    $user->setPhotoPath($fileName);
                }
                $em->persist($user);
                $em->flush();
                $succes = 'Данные успешно изменены';
            }
        }
        $user->setUploadAvatar(null);
        $changePasswordForm = $this->createForm(ChangePasswordType::class);
        $changePasswordForm->handleRequest($request);


        $errorsPass = null;
        if ($changePasswordForm->isSubmitted()) {
            if ($changePasswordForm->isValid()) {
                $encoder_service = $this->get('security.encoder_factory');
                $encoder = $encoder_service->getEncoder($user);
                $password = $changePasswordForm->getData()["actualPass"];
                $isValid = $encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt());
                if ($isValid) {
                    $messagePass=$changePasswordForm->getData()["password"];
                    $password = $this
                        ->get('security.password_encoder')
                        ->encodePassword($user, $changePasswordForm->getData()["password"]);
                    $user->setPassword($password);
                    $em->persist($user);
                    $em->flush();
                    $message = (new \Swift_Message('Успешное изменение пароля!'))
                        ->setFrom('nmbakhtiyarov@gmail.com')
                        ->setTo($user->getEmail())
                        ->setBody(
                            $this->renderView(
                                'email/mail.html.twig',[
                                    'uname'=>$user->getFirstName(),
                                    'pass'=>$messagePass,
                                    'login'=>$user->getEmail()
                                ]
                            ),
                            'text/html'
                        )
                    ;
                    $mailer->send($message);
                    $passSucces = 'Пароль успешно изменен';
                } else {
                    $errorsPass = "Неверный пароль";
                }

            } else
                $errorsPass = "Пароли не совпадают";
        }
        return $this->render('pages/cabinet/settings.html.twig', [
            'form' => $form->createView(),
            'errors' => $errors,
            'formPass' => $changePasswordForm->createView(),
            'errorsPass' => $errorsPass,
            'formSucces' => $succes,
            'passSucces' => $passSucces
        ]);
    }
}