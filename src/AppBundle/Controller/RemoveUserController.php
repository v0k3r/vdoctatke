<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 27.01.18
 * Time: 3:32
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RemoveUserController
{

    public function query(Request $request,EntityManagerInterface $em)
    {
        $support=$em->getRepository("AppBundle:User")->findOneByEmail("support@admin.ru");
       // $guests=$em->getRepository("AppBundle:User")->getIdByRole("ROLE_GUEST");
        //var_dump($guests);
        $qb = $em->createQueryBuilder();
        $qb->select('m')
            ->from('AppBundle:Message', 'm')
            ->where('m.date < :last')
            ->andWhere('m.from=:support')
            ->andWhere("m.to.getRoles like '%ROLE_GUEST%' ")
            ->setParameter('last', new \DateTime('-30 days'), \Doctrine\DBAL\Types\Type::DATETIME)
            ->setParameter('support', $support);


        $oldUsers = $qb->getQuery()->getResult();


       return new Response("123");
    }
    /**
     * @Route("/cabinet/regdata", name="adddata")
     */
    public function addDataAction(Request $request,EntityManagerInterface $em)
    {
        $support=$em->getRepository("AppBundle:User")->findOneByEmail("support@admin.ru");
        $messages=$em->getRepository("AppBundle:Message")->findByText("Добрый день! Служба технической поддержки команды VDOCTATKE - к вашим услугам!");
        /**
         * @var $message Message
         */
        foreach ($messages as $message) {
            /**
             * @var $user User
             */
            $user=$message->getTo();
            $user->setRegistratedAt($message->getDate());
            $em->persist($user);
        }
        $em->flush();

        return new Response(count($messages));
    }
    /**
     * @Route("/cabinet/removeOldUsers", name="remove-old")
     */
    public function removeAction(Request $request,EntityManagerInterface $em)
    {
        $qb = $em->createQueryBuilder();
        $qb->select('u')
            ->from('AppBundle:User', 'u')
            ->where('u.registratedAt < :last')
            ->andWhere("u.roles like '%ROLE_GUEST%' ")
            ->setParameter('last', new \DateTime('-30 days'), \Doctrine\DBAL\Types\Type::DATETIME)
            ;


        $oldUsers = $qb->getQuery()->getResult();
         foreach ($oldUsers as $user){
             $em->getRepository("AppBundle:User")->removeGuestUser($user);
         }

        return new Response(count($oldUsers));
    }
}