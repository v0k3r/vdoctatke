<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 28.08.17
 * Time: 19:33
 */

namespace AppBundle\Controller;
use AppBundle\Entity\MainVideo;
use AppBundle\Form\MainVideoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class MainVideoController extends Controller
{
    /**
     * @Route("/cabinet/video/update/{type}",name="example")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateVideo(Request $request,$type)
    {
        $em=$this->getDoctrine()->getManager();
        /**
         * @var MainVideo $video
         */
        $video=$em->getRepository('AppBundle:MainVideo')->findOneByType($type);
        $form = $this->createForm(MainVideoType::class, $video);
       $form->handleRequest($request);
        if ($form->isSubmitted()&&$form->isValid()){
            $vid=$video->getVideoId();
            if( strstr($vid,'youtu.be')){
                $start=strripos($vid, 'youtu.be/')+9;
                $end=strripos($vid, '?');
                if ($end) {
                    $length=$end-$start;
                    $vid = substr($vid, $start,$length  );
                }
                else $vid = substr($vid, $start);
            }
            elseif(strstr($vid,'youtube')){
                $start=strripos($vid, '?v=')+3;
                $end=strripos($vid, '&');
                if ($end) {
                    $length=$end-$start;
                    $vid = substr($vid, $start,$length);
                }
                else $vid = substr($vid, $start);
            }
            $video->setVideoId($vid);
            if ($type=='training'){
                $video->setDate(new \DateTime('now'));
            }
            $em->persist($video);
            $em->flush();
            if ($type=='main') {
                return $this->redirectToRoute('cabinet_client');
            }
            elseif($type=='marketing'){
                return $this->redirectToRoute('cabinet_marketing');
            }
            elseif ($type=='training'){
                return $this->redirectToRoute('cabinet_training');
            }
        }
        return $this->render('pages/video/update.html.twig',[
           'form'=>$form->createView()
        ]);
    }

}