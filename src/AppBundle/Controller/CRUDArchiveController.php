<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 01.10.17
 * Time: 4:53
 */

namespace AppBundle\Controller;

use AppBundle\DTO\CreateFolder;
use AppBundle\Entity\ArchiveFile;
use AppBundle\Form\CreateArchiveFileType;
use AppBundle\Form\CreateFolderType;
use AppBundle\Repository\ArchiveRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CoreController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
class CRUDArchiveController extends CoreController
{
    /**
     * @Route("/admin/main/archive/{parent}", name="admin_show_archive")
     * @param null $parent
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction($parent = null, Request $request, EntityManagerInterface $entityManager)
    {
        $parent = (!$parent) ? null : $parent;
        $current = $parent;
        $archiveRepository = $entityManager->getRepository("AppBundle:ArchiveFile");
//        $files = $archiveRepository->findAll();
        $children = [];
        $prev = null;
        if ($parent != null) {
            /** @var ArchiveFile $currentFolder */
            $currentFolder = $archiveRepository->find($current);
            $children = $currentFolder->getChildren();
            if (is_null($currentFolder->getParent())) {
                $prev = 0;
            } else {
                $prev = $currentFolder->getParent()->getId();
            }
        } else {
            $children = $archiveRepository->findBy([
                'parent' => null
            ]);
        }

        $archiveFolderDTO = new CreateFolder();
        $formCreateFolder = $this->createForm(CreateFolderType::class, $archiveFolderDTO, [
            'action' => $this->generateUrl('admin-create_folder', ['parent' => $parent]),
            'method' => "POST"
        ]);

        $formCreateFolder->handleRequest($request);
        if (is_array($children)){
            $children=array_reverse($children);
        }
        elseif (count($children)>1){
            $children =array_reverse($children->getValues());
        }
//        $children = (is_array($children)) ? array_reverse($children) : $children;

        return $this->render('CRUD/custompage.html.twig', array(
            'base_template' => $this->getBaseTemplate(),
            'admin_pool' => $this->container->get('sonata.admin.pool'),
            'blocks' => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'),
            'formFolderCreate' => $formCreateFolder->createView(),
            'children' => $children,
            'root' => (is_null($parent)) ? true : false,
            'prev' => $prev,
            'parent'=>$parent
        ));

    }
    /**
     * @Route("/admin/main/app/archivefile/create/{parent}", name="admin-create_file")
     * @param null $parent
     * @param Request $request
     * @return string
     */
    public function createAction($parent = null, Request $request, EntityManagerInterface $entityManager){
        /** @var ArchiveRepository $archiveRepository */
        $archiveRepository = $entityManager->getRepository("AppBundle:ArchiveFile");
        $file=new ArchiveFile();
        $parentFolder=$parent;
        $formCreateFile = $this->createForm(CreateArchiveFileType::class, $file);
        $formCreateFile->handleRequest($request);
        if ($formCreateFile->isSubmitted() && $formCreateFile->isValid()) {
            if ($file->getName()==''){
                $file->setName('Новый файл');
            }
            if ($parent!=null){
                $parentFolder=$entityManager->getRepository('AppBundle:ArchiveFile')->find($parent);
            }
            $file->setType('Файл')->setIsDirectory(false)->setParent($parentFolder);
            $entityManager->persist($file);
            $entityManager->flush();

            return $this->redirectToRoute('admin_show_archive', [
                'parent' => $parent
            ]);
        }
        return $this->render('CRUD/create-archivefile.html.twig',[
            'form'=>$formCreateFile->createView()
        ]);
    }

    /**
     * @Route("/admin/main/app/file/remove/{id}", name="admin-remove_file")
     * @param $id
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function removeItem($id, Request $request, EntityManagerInterface $entityManager)
    {
        /** @var ArchiveRepository $archiveRepository */
        $archiveRepository = $entityManager->getRepository("AppBundle:ArchiveFile");
        /** @var ArchiveFile $item */
        $item = $archiveRepository->find($id);
        $parent = $item->getParent();
        $item->setParent(null);
        if ($parent!=null) {
            $parent->removeChild($item);
            $entityManager->persist($parent);
        }
        $entityManager->remove($item);
        $entityManager->flush();
        $redirect = null;
        if (!is_null($parent)) {
            $redirect = $parent->getId();
        }
        return $this->redirectToRoute('admin_show_archive', [
            'parent' =>$redirect
        ]);
    }

    /**
     * @Route("/admin/main/app/archive/redactfile/{id}", name="admin-redact_file")
     * @param $id
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function redactFile($id, Request $request, EntityManagerInterface $entityManager)
    {
        /** @var ArchiveRepository $archiveRepository */
        $archiveRepository = $entityManager->getRepository("AppBundle:ArchiveFile");
        $file=$archiveRepository->find($id);

        $formCreateFile = $this->createForm(CreateArchiveFileType::class, $file);
        $formCreateFile->handleRequest($request);
        if ($formCreateFile->isSubmitted() && $formCreateFile->isValid()) {
            if ($file->getName()==''){
                $file->setName('Новый файл');
            }
            $entityManager->persist($file);
            $entityManager->flush();
            $parent=$file->getParent();
            if ($parent==null){
                $id=0;
            }
            else{
                $id=$parent->getId();
            }
            return $this->redirectToRoute('admin_show_archive', [
                'parent' => $id
            ]);
        }
        return $this->render('CRUD/create-archivefile.html.twig',[
            'form'=>$formCreateFile->createView()
        ]);
    }
    /**
     * @Route("/admin/main/app/folder/{parent}", name="admin-create_folder")
     * @param null $parent
     * @param Request $request
     * @return string
     */
    public function createFolder($parent = null, Request $request, EntityManagerInterface $entityManager)
    {
        $archiveRepository = $entityManager->getRepository("AppBundle:ArchiveFile");
        $archiveFolderDTO = new CreateFolder();
        $formCreateFolder = $this->createForm(CreateFolderType::class, $archiveFolderDTO);
        $formCreateFolder->handleRequest($request);
        if ($formCreateFolder->isSubmitted() && $formCreateFolder->isValid()) {
            $folder = new ArchiveFile();
            if (!$parent) {
                $folder->setParent(null);
            } else {
                $parentEntity = $archiveRepository->find($parent);
                $folder->setParent($parentEntity);
            }
            $folder->setIsDirectory(true);
            if ($archiveFolderDTO->name==''){
                $folder->setName("Новая папка");
            }
            else{
                $folder->setName($archiveFolderDTO->name);
            }

            $folder->setType('D');
            $entityManager->persist($folder);
            $entityManager->flush();
            return $this->redirectToRoute('admin_show_archive', [
                'parent' => $folder->getId()
            ]);
        }
        return $this->redirectToRoute('admin_show_archive', [
            'parent' => $parent
        ]);
    }
    /**
     * @Route("/admin/main/app/archive/showfile/{id}", name="admin-show_file")
     * @param $id
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function showFile($id, Request $request, EntityManagerInterface $entityManager)
    {
        /** @var ArchiveRepository $archiveRepository */
        $archiveRepository = $entityManager->getRepository("AppBundle:ArchiveFile");
        $file=$archiveRepository->find($id);
        return $this->render("CRUD/show-archive.html.twig",[
            'file'=>$file
        ]);
    }

}