<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 20.06.17
 * Time: 0:49
 */

namespace AppBundle\Controller;

use AppBundle\DTO\CreateLanding;
use AppBundle\Entity\User;
use AppBundle\Form\CreateLandingType;
use AppBundle\Repository\UserRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class CabinetController extends Controller
{

    /**
     * @Route("/cabinet/wait", name="cabinet_wait")
     */
    public function waitAction(Request $request)
    {
        /**
         * @var $user User
         */
        $user = $this->getUser();

        if ($request->isXmlHttpRequest() && !$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $token = $this->get('security.token_storage')->getToken();
            $token->setAuthenticated(false);
            $this->get('session')->migrate();
            return new JsonResponse([
                'url' => $this->generateUrl('cabinet_create-landing')
            ]);
        } elseif ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'ok' => 1
            ]);
        }
        if ($this->get('security.authorization_checker')->isGranted('ROLE_NEW_PARTNER')) {
            $token = $this->get('security.token_storage')->getToken();
            $token->setAuthenticated(false);
            $this->get('session')->migrate();
            return new RedirectResponse($this->generateUrl('cabinet_create-landing'));
        }
        return $this->render('pages/cabinet/wait.html.twig');
    }

    /**
     * @Route("/cabinet/client/course", name="cabinet_client_course")
     */
    public function showClienCourse(Request $request)
    {
        return $this->render('pages/cabinet/client.course.html.twig');
    }


    /**
     * @Route("/cabinet/faq", name="cabinet_faq")
     */
    public function showClientFaq(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $faq = $em->getRepository('AppBundle:Faq')->findAll();
        return $this->render('pages/cabinet/faq.html.twig', [
            'faqs' => $faq
        ]);
    }

    /**
     * @Route("/cabinet/marketing", name="cabinet_marketing")
     */
    public function showClientMarketing(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository('AppBundle:MainVideo')->findOneByType('marketing');
        return $this->render('pages/cabinet/client.main.html.twig', [
            'video' => $video
        ]);
    }

    /**
     * @Route("/cabinet/client", name="cabinet_client")
     */
    public function showClienMain(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $mainVideo = $em->getRepository('AppBundle:MainVideo')->findOneByType('main');
        return $this->render('pages/cabinet/client.main.html.twig', [
            'video' => $mainVideo
        ]);
    }

    /**
     * @Route("/cabinet/training", name="cabinet_training")
     */
    public function showTrainingVideo(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository('AppBundle:MainVideo')->findOneByType('training');
        /**
         * @var User $user
         */
        $user = $this->getUser();
        $user->setLastTrainingVisit(new \DateTime('now'));
        $em->persist($user);
        $em->flush();
        return $this->render('pages/cabinet/client.main.html.twig', [
            'video' => $video
        ]);
    }

    /**
     * @Route("/cabinet/getuser/{id}")
     * @param Request $request
     * @param $id
     * @param EntityManagerInterface $entityManager
     * @return string
     */
    public function getUserId(Request $request, $id, EntityManagerInterface $entityManager)
    {
        $userRepository = $entityManager->getRepository('AppBundle:User');
        /** @var User $user */
        $user = $userRepository->find($id);
        $criteria = Criteria::create()->andWhere(Criteria::expr()->orX(
            Criteria::expr()->contains('roles', 'ROLE_PARTNER'),
            Criteria::expr()->contains('roles', 'ROLE_ADMIN')
        ));
        $view = $this->renderView('parts/structure.info.html.twig', [
            'user' => [
                'id' => $user->getId(),
                'name' => $user->getFirstName() . " " . $user->getLastName() . " (" . $user->getCity() . ")",
                'phone' => $user->getTel(),
                'idso' => $this->getIntFromStr($user->getSureoneId()),
                'vk' => $user->getVk(),
                'whatsapp' => $user->getWhatsup(),
                'telegram' => $user->getTelegram(),
                'personalCount' => $userRepository->countPersonalRefs($user),
                'structCount' => count($userRepository->getChildEntities([$user->getId()], array(), $criteria)),
                'stat' => $this->calcStats($user),
                'photo' => $user->getPhotoPath(),
                'roles' => $user->getRoles(),
                'lan' => $user->getLandingName(),


            ]
        ]);
        return new JsonResponse([
            'view' => $view
        ]);
    }
    /**
     * @Route("/cabinet/getcurator/{id}")
     * @param Request $request
     * @param $id
     * @param EntityManagerInterface $entityManager
     * @return string
     */
    public function getCuratorId(Request $request, $id, EntityManagerInterface $entityManager)
    {
        $userRepository = $entityManager->getRepository('AppBundle:User');
        /** @var User $user */
        $user = $userRepository->find($id);
        $view = $this->renderView('parts/curators.info.html.twig', [
            'user' => [
                'id' => $user->getId(),
                'name' => $user->getFirstName() . " " . $user->getLastName() . " (" . $user->getCity() . ")",
                'phone' => $user->getTel(),
                'idso' => $this->getIntFromStr($user->getSureoneId()),
                'vk' => $user->getVk(),
                'whatsapp' => $user->getWhatsup(),
                'telegram' => $user->getTelegram(),

                'photo' => $user->getPhotoPath(),
                'roles' => $user->getRoles(),
                'lan' => $user->getLandingName(),


            ]
        ]);
        return new JsonResponse([
            'view' => $view
        ]);
    }

    /**
     * @Route("/cabinet/stat", name="cabinet_stat")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showStat(Request $request, EntityManagerInterface $entityManager)
    {
        /**
         * @var $user User
         */
        $user = $this->getUser();
        $userRepository = $entityManager->getRepository('AppBundle:User');
//        $common =
        return $this->render('pages/cabinet/stats.html.twig', [
            'personal' => $this->calcStats($user),
            'structure' => $this->calcStructureStat($user)
        ]);
    }

    private function calcStructureStat($user)
    {
        $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->orX(
                Criteria::expr()->contains('roles', 'ROLE_PARTNER'),
                Criteria::expr()->contains('roles', 'ROLE_ADMIN')
            ));
        $childs = $userRepository->getChildEntities($user, [], $criteria);
        $res = [];
        foreach ($childs as $child) {
            $res[] = $this->calcStats($child);
        }
        $res[] = $this->calcStats($user);
        $final = array();

        array_walk_recursive($res, function ($item, $key) use (&$final) {
            $final[$key] = isset($final[$key]) ? $item + $final[$key] : $item;
        });
        return $final;
    }

    private function calcStats(User $user)
    {
        $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
        return [
            'views' => $user->getViews(),
            'leftData' => $userRepository->countReferalsLeftData($user),
            'interested' => $userRepository->countInterestedRefs($user),
            'payed' => $userRepository->countPayed($user),
            'other' => $userRepository->countOtherRef($user)
        ];
    }

    /**
     * @Route("/cabinet/requests", name="cabinet_requests")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userRequests(Request $request, EntityManagerInterface $entityManager)
    {
        $currentUser = $this->getUser();
        $myRequests = $entityManager->getRepository('AppBundle:User')->getUserRequests($currentUser);
        return $this->render('pages/cabinet/requests.html.twig', [
            'requests' => $myRequests
        ]);
    }

    /**
     * @Route("/cabinet/req/accept/{reqId}", name="accept-request")
     * @param $reqId
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function acceptRequest($reqId, Request $request, EntityManagerInterface $entityManager)
    {
        $currentUser = $this->getUser();
        $userRepository = $entityManager->getRepository('AppBundle:User');
        /**
         * @var $targetUser User
         */
        $targetUser = $userRepository->find($reqId);
        if (!$userRepository->isUserParent($targetUser, $currentUser)) {
            throw $this->createNotFoundException(
                'No requests found with id  ' . $reqId
            );
        }
        $targetUser->setRoles(['ROLE_NEW_PARTNER']);
        $targetUser->setConfirmed($currentUser);
        $entityManager->flush();
        $token = $this->get('security.token_storage')->getToken();
        $token->setAuthenticated(false);
        $this->get('session')->migrate();
        return new JsonResponse(['status' => 1]);
    }

    /**
     * @Route("/cabinet/req/block/{reqId}", name="block-user")
     * @param $reqId
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function blockUser($reqId, Request $request, EntityManagerInterface $entityManager)
    {
        $currentUser = $this->getUser();
        $userRepository = $entityManager->getRepository('AppBundle:User');
        /**
         * @var $targetUser User
         */
        $targetUser = $userRepository->find($reqId);
        $role = $currentUser->getRoles();
        if (!in_array('ROLE_ADMIN', $role)) {
            return new JsonResponse(['status' => 0], 500);
        }

        if (in_array('ROLE_BANNED', $targetUser->getRoles())) {
            $targetUser->setRoles(["ROLE_PARTNER"]);
        } else {
            $targetUser->setRoles(['ROLE_BANNED']);
        }
        $entityManager->flush();
        $token = $this->get('security.token_storage')->getToken();
        $token->setAuthenticated(false);
        $this->get('session')->migrate();
        return new JsonResponse(['status' => 1]);
    }

    /**
     * @Route("/cabinet/req/reject/{reqId}", name="reject-request")
     * @param $reqId
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function removeUser($reqId, Request $request, EntityManagerInterface $entityManager)
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $userRepository = $entityManager->getRepository('AppBundle:User');
        /**
         * @var $targetUser User
         */
        $targetUser = $userRepository->find($reqId);
        if ($targetUser == $this->getUser()) {
            $token = $this->get('security.token_storage')->getToken();
            $token->setAuthenticated(false);
            $this->get('session')->clear();
            $userRepository->removeUser($targetUser);
            $entityManager->flush();
        }
        if ($currentUser->getId() != $targetUser->getId()) {
            if (!$userRepository->isUserParent($targetUser, $currentUser)) {
                throw $this->createNotFoundException(
                    'No requests found with id  ' . $reqId
                );
            }
        }

        $userRepository->removeUser($targetUser);
        $entityManager->flush();
        return new JsonResponse(['status' => 1]);
    }

    /**
     * @Route("/cabinet/structure", name="cabinet-structure")
     */
    public function structurePage(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            return $this->tree();
        }
        $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
        $user = $this->getUser();
        $criteria = Criteria::create()->andWhere(Criteria::expr()->orX(
            Criteria::expr()->contains('roles', 'ROLE_PARTNER'),
            Criteria::expr()->contains('roles', 'ROLE_ADMIN')
        ));

        return $this->render('pages/cabinet/structure.html.twig', [
            'user' => [
                'id' => $user->getId(),
                'name' => $user->getFirstName() . " " . $user->getLastName(),
                'phone' => $user->getTel(),
                'idso' => $this->getIntFromStr($user->getSureoneId()),
                'vk' => $user->getVk(),
                'whatsapp' => $user->getWhatsup(),
                'telegram' => $user->getTelegram(),
                'personalCount' => $userRepository->countPersonalRefs($user),
                'structCount' => count($userRepository->getChildEntities([$user->getId()], array(), $criteria)),
                'stat' => $this->calcStats($user),
                'photo' => $user->getPhotoPath(),
                'roles' => $user->getRoles(),
                'lan' => $user->getLandingName(),


            ]
        ]);
    }

    /**
     * @Route("/cabinet/curators", name="cabinet-curators")
     */
    public function curatorsStructurePage(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            return $this->curatorsTree();
        }
        $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
        /**
         * @var User $user
         */
        $user = $this->getUser();
        $parent = $user->getParent();
        if ($parent == null) {
            return $this->render("pages/cabinet/no-curators.html.twig");
        }
        for ($i = 0; $i < 5; $i++) {

            if ($parent->getParent() != null) {
                $parent = $parent->getParent();
            } else {
                break;
            }
        }
        $criteria = Criteria::create()->andWhere(Criteria::expr()->orX(
            Criteria::expr()->contains('roles', 'ROLE_PARTNER'),
            Criteria::expr()->contains('roles', 'ROLE_ADMIN')
        ));

        return $this->render('pages/cabinet/curator-structure.html.twig', [
            'user' => [
                'id' => $parent->getId(),
                'name' => $parent->getFirstName() . " " . $parent->getLastName(),
                'phone' => $parent->getTel(),
                'idso' => $this->getIntFromStr($parent->getSureoneId()),
                'vk' => $parent->getVk(),
                'whatsapp' => $parent->getWhatsup(),
                'telegram' => $parent->getTelegram(),
                'personalCount' => $userRepository->countPersonalRefs($parent),
                'structCount' => count($userRepository->getChildEntities([$parent->getId()], array(), $criteria)),
                'stat' => $this->calcStats($parent),
                'photo' => $parent->getPhotoPath(),
                'roles' => $parent->getRoles(),
                'lan' => $parent->getLandingName(),


            ]
        ]);
    }

    private function getIntFromStr($str)
    {


        if ($str != null) {
            preg_match_all('|\d+|', $str, $matches);

            if ($matches[0])
                return $matches[0][0];
            else return "Не определен";
        } else return "Не определен";
    }

    private function tree()
    {
        /** @var User $me */
        $me = $this->getUser();
        /** @var UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
        $criteria = Criteria::create()->andWhere(Criteria::expr()->orX(
            Criteria::expr()->contains('roles', 'ROLE_PARTNER'),
            Criteria::expr()->contains('roles', 'ROLE_ADMIN'),
            Criteria::expr()->contains('roles', 'ROLE_BANNED')
        ));
        $users = $userRepository->getChildEntities($me, array(), $criteria);
        $rows = [];
        $rows[] = [
            'id' => '' . $me->getId(),
            'parent' => '#',
            'text' => $me->getFirstName() . ' ' . $me->getLastName(),
            'icon' => (!$me->getPhotoPath()) ? '/img/av-zagl.png' : '/photos' . '/' . $me->getPhotoPath()
        ];
        /** @var User $user */
        foreach ($users as $user) {
            $rows[] = [
                'id' => '' . $user->getId(),
                'parent' => '' . $user->getParent()->getId(),
                'text' => $user->getFirstName() . ' ' . $user->getLastName() . ' (' . $user->getCity() . ')',
                'icon' => (!$user->getPhotoPath()) ? '/img/av-zagl.png' : '/photos' . '/' . $user->getPhotoPath()
            ];
        }
        return new JsonResponse($rows);

    }

    private function curatorsTree()
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();
        $parent[] = $user->getParent();
        if ($parent[0] != null) {


            for ($i = 0; $i < 5; $i++) {

                if ($parent[count($parent) - 1]->getParent() != null) {
                    $parent[] = $parent[count($parent) - 1]->getParent();
                } else {
                    break;
                }
            }
        }

//        $users[]=$this->getUser()->getParent()->getParent()->getParent();
//        $users[]=$this->getUser()->getParent()->getParent()->getParent()->getParent();

        $me = $this->getUser();
//        $rows[] = [
//            'id' => '' . $me->getId(),
//            'parent' => '#',
//            'text' => $me->getFirstName() . ' ' . $me->getLastName(),
//            'icon' => (!$me->getPhotoPath()) ? '/img/av-zagl.png' : '/photos' . '/' . $me->getPhotoPath()
//        ];
        /** @var User $user */
        $parent=array_reverse($parent);
        $i=0;
        foreach ($parent as $user) {
            if ($i==0)
            $rows[] = [
                'id' => $user->getId(),
                'parent' => '#',
                'text' => $user->getFirstName()." ".$user->getLastName(),
                'icon' => '/photos/'.$user->getPhotoPath()
            ];
            else
                $rows[] = [
                    'id' => $user->getId(),
                    'parent' => $user->getParent()->getId(),
                    'text' => $user->getFirstName()." ".$user->getLastName(),
                    'icon' => '/photos/'.$user->getPhotoPath()
                ];
            $i++;
        }
//        $rows[] = [
//            'id' => '140' ,
//            'parent' => '#',
//            'text' => "kolya",
//            'icon' => '/img/av-zagl.png'
//        ];
//        $rows[] = [
//            'id' => '11' ,
//            'parent' => '140',
//            'text' => "kolya",
//            'icon' => '/img/av-zagl.png'
//        ];
//        $rows[] = [
//            'id' => '11' ,
//            'parent' => '140',
//            'text' => "kolya",
//            'icon' => '/img/av-zagl.png'
//        ];
//        $rows[] = [
//            'id' => '11' ,
//            'parent' => '140',
//            'text' => "kolya",
//            'icon' => '/img/av-zagl.png'
//        ];
//        $rows[] = [
//            'id' => '11' ,
//            'parent' => '140',
//            'text' => "kolya",
//            'icon' => '/img/av-zagl.png'
//        ];
//        foreach ($users as $user) {
//            $rows[] = [
//                'id' => '' . $user->getId(),
//                'parent' => '' . $user->getParent()->getId(),
//                'text' => $user->getFirstName() . ' ' . $user->getLastName() . ' (' . $user->getCity(). ')',
//                'icon' => (!$user->getPhotoPath()) ? '/img/av-zagl.png' : '/photos' . '/' . $user->getPhotoPath()
//            ];
//        }
        return new JsonResponse($rows);

    }

    /**
     * @Route("/cabinet/brokertiki", name="cabinet_brokertiki")
     */
    public function showbrokerTiki(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository('AppBundle:MainVideo')->findOneByType('brokertiki');
        return $this->render('pages/cabinet/brokertiki.html.twig', [
            'video' => $video
        ]);
    }

    /**
     * @Route("/cabinet/faststart", name="cabinet_faststart")
     */
    public function showFs(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository('AppBundle:MainVideo')->findOneByType('faststart');
        return $this->render('pages/cabinet/client.main.html.twig', [
            'video' => $video
        ]);
    }

    /**
     * @Route("/cabinet/myguest", name="guest_list")
     */
    public function showGuestsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $guests = $em->getRepository('AppBundle:User')->findByRoleAndParent("ROLE_GUEST", $user);
        return $this->render('pages/cabinet/guests.html.twig', [
            'guests' => $guests
        ]);
    }

}