<?php
// src/AppBundle/Controller/CRUDController.php

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as BaseController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Exception\LockException;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CRUDController extends BaseController
{

    /**
     * @param ProxyQueryInterface $selectedModelQuery
     * @param Request $request
     * @return RedirectResponse
     */
    public function batchActionEmail(ProxyQueryInterface $selectedModelQuery, Request $request = null)
    {
        $this->admin->checkAccess('edit');
        $this->admin->checkAccess('delete');
        $modelManager = $this->admin->getModelManager();
        $selectedModels = $selectedModelQuery->execute();

        $mailer=$this->get('mailer');
        try {

            $text=$request->request->get('text');
            $title=$request->request->get('title');
            foreach ($selectedModels as $key=>$user) {
                try {
                    //send message with password
                    $message = (new \Swift_Message($title))
                        ->setFrom('nmbakhtiyarov@gmail.com')
                        ->setTo($user->getEmail())
                        ->setBody(
                            $this->renderView(
                                'email/admin-dispatch.html.twig',[
                                    'uname'=>$user->getFirstName(),
                                    'login'=>$user->getEmail(),
                                    'text'=>$text,
                                ]
                            ),
                            'text/html'
                        )
                    ;
                    $mailer->send($message);

                } catch (\Exception $e) {
                    $this->addFlash('sonata_flash_error', $e->getMessage());
                    return new RedirectResponse(
                        $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
                    );
                }

            }


        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $e->getMessage());

            return new RedirectResponse(
                $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
            );
        }

        $this->addFlash('sonata_flash_success', 'Сообщения разосланы');



        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
        );
    }

    public function batchActionMessageIsRelevant(array $selectedIds, $allEntitiesSelected, Request $request = null)
    {
        // here you have access to all POST parameters, if you use some custom ones
        // POST parameters are kept even after the confirmation page.
        $parameterBag = $request->request;



        $targetId = $parameterBag->get('targetId');

        // if all entities are selected, a merge can be done
        if ($allEntitiesSelected) {
            return true;
        }

        // filter out the target from the selected models
        $selectedIds = array_filter($selectedIds,
            function($selectedId) use($targetId){
                return $selectedId !== $targetId;
            }
        );

        // if at least one but not the target model is selected, a merge can be done.
        return count($selectedIds) > 0;
    }

    /**
     * Delete action.
     *
     * @param int|string|null $id
     * @return Response|RedirectResponse
     * @throws NotFoundHttpException If the object does not exist
     * @throws AccessDeniedException If access is not granted
     */
    public function deleteAction($id)
    {
        $em=$this->getDoctrine()->getManager();

        $request = $this->getRequest();
        $id = $request->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw $this->createNotFoundException(sprintf('Не получилось найти юзера с Id : %s', $id));
        }

        $this->admin->checkAccess('delete', $object);

        $preResponse = $this->preDelete($request, $object);
        if ($preResponse !== null) {
            return $preResponse;
        }

        if ($this->getRestMethod() == 'DELETE') {
            // check the csrf token
            $this->validateCsrfToken('sonata.delete');

            $objectName = $this->admin->toString($object);

            try {
                $em->getRepository('AppBundle:User')->removeUser($object);
                $em->flush();
//                $this->admin->delete($object);

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array('result' => 'ok'), 200, array());
                }

                $this->addFlash(
                    'sonata_flash_success',
                    $this->trans(
                        'flash_delete_success',
                        array('%name%' => $this->escapeHtml($objectName)),
                        'SonataAdminBundle'
                    )
                );
            } catch (ModelManagerException $e) {
                $this->handleModelManagerException($e);

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array('result' => 'error'), 200, array());
                }

                $this->addFlash(
                    'sonata_flash_error',
                    $this->trans(
                        'flash_delete_error',
                        array('%name%' => $this->escapeHtml($objectName)),
                        'SonataAdminBundle'
                    )
                );
            }

            return $this->redirectTo($object);
        }

        return $this->render($this->admin->getTemplate('delete'), array(
            'object' => $object,
            'action' => 'delete',
            'csrf_token' => $this->getCsrfToken('sonata.delete'),
        ), null);
    }

    /**
     * Execute a batch delete.
     * @param ProxyQueryInterface $selectedModelQuery
     * @return RedirectResponse
     * @throws AccessDeniedException If access is not granted
     */
    public function batchActionDelete(ProxyQueryInterface $selectedModelQuery)
    {

        $this->admin->checkAccess('edit');
        $this->admin->checkAccess('delete');
        $modelManager = $this->admin->getModelManager();
        $em=$this->getDoctrine()->getManager();
        $selectedModels = $selectedModelQuery->execute();


        try {
            foreach ($selectedModels as $key=>$user) {
                try {

                    $em->getRepository('AppBundle:User')->removeUser($user);

                } catch (\Exception $e) {
                    $this->addFlash('sonata_flash_error', $e->getMessage());
                    return new RedirectResponse(
                        $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
                    );
                }

            }

            $em->flush();
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $e->getMessage());

            return new RedirectResponse(
                $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
            );
        }

        $this->addFlash('sonata_flash_success', 'Пользователи удалены');



        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
        );
    }
    /**
     * Edit action.
     *
     * @param int|string|null $id
     *
     * @return Response|RedirectResponse
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws AccessDeniedException If access is not granted
     */
    public function editAction($id = null)
    {
        $em=$this->getDoctrine()->getManager();
        $userRepository=$em->getRepository('AppBundle:User');
        $request = $this->getRequest();
        // the key used to lookup the template
        $templateKey = 'edit';

        $id = $request->get($this->admin->getIdParameter());
        $existingObject = $this->admin->getObject($id);

        if (!$existingObject) {
            throw $this->createNotFoundException(sprintf('Не получилось найти пользователя с id : %s', $id));
        }
        $oldPass=$existingObject->getPassword();
        $oldPhoto=$existingObject->getPhotoPath();
        $this->admin->checkAccess('edit', $existingObject);

        $preResponse = $this->preEdit($request, $existingObject);
        if ($preResponse !== null) {
            return $preResponse;
        }

        $this->admin->setSubject($existingObject);

        /** @var $form Form */
        $form = $this->admin->getForm();
        $form->setData($existingObject);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            //TODO: remove this check for 4.0
            if (method_exists($this->admin, 'preValidate')) {
                $this->admin->preValidate($existingObject);
            }
            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {


                $submittedObject = $form->getData();
                $this->admin->setSubject($submittedObject);

                try {
                    $photo=$submittedObject->getUploadAvatar();
                    if ($photo!=null)
                    {
                        $savedPhoto = $userRepository->saveUserPhoto($photo, $submittedObject, $this->get('kernel')->getRootDir());
                        $submittedObject->setPhotoPath($savedPhoto);
                    }
                    else{
                        $submittedObject->setPhotoPath($oldPhoto);
                    }
                    if ($submittedObject->getPassword()==null){
                        $submittedObject->setPassword($oldPass);
                    }
                    else{
                        $pass = $this->get('security.password_encoder')->encodePassword($submittedObject, $submittedObject->getPassword());
                        $submittedObject->setPassword($pass);
                    }
                    $submittedObject->setUploadAvatar(null);
                    $existingObject = $this->admin->update($submittedObject);

                    if ($this->isXmlHttpRequest()) {
                        return $this->renderJson(array(
                            'result' => 'ok',
                            'objectId' => $this->admin->getNormalizedIdentifier($existingObject),
                            'objectName' => $this->escapeHtml($this->admin->toString($existingObject)),
                        ), 200, array());
                    }

                    $this->addFlash(
                        'sonata_flash_success',
                        $this->trans(
                            'flash_edit_success',
                            array('%name%' => $this->escapeHtml($this->admin->toString($existingObject))),
                            'SonataAdminBundle'
                        )
                    );

                    // redirect to edit mode
                    return $this->redirectTo($existingObject);
                } catch (ModelManagerException $e) {
                    $this->handleModelManagerException($e);

                    $isFormValid = false;
                } catch (LockException $e) {
                    $this->addFlash('sonata_flash_error', $this->trans('flash_lock_error', array(
                        '%name%' => $this->escapeHtml($this->admin->toString($existingObject)),
                        '%link_start%' => '<a href="'.$this->admin->generateObjectUrl('edit', $existingObject).'">',
                        '%link_end%' => '</a>',
                    ), 'SonataAdminBundle'));
                }
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                $existingObject->setUploadAvatar(null);
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash(
                        'sonata_flash_error',
                        $this->trans(
                            'flash_edit_error',
                            array('%name%' => $this->escapeHtml($this->admin->toString($existingObject))),
                            'SonataAdminBundle'
                        )
                    );
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        }

        $formView = $form->createView();
        // set the theme for the current Admin Form


        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit',
            'form' => $formView,
            'object' => $existingObject,
        ), null);
    }
}