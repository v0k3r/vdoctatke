<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 30.08.17
 * Time: 4:09
 */

namespace AppBundle\Admin;

use AppBundle\Form\ImageType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class CourseAdmin extends AbstractAdmin
{

    public function configure()
    {
        parent::configure();
        $this->classnameLabel = "Курсы";
    }
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array('label' => 'Название'))
        ->add('photoPath')
        ->add('uploadImage',ImageType::class, array('required' => false))
        ->add('videoId', null, array('label' => 'Ссылка'));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array('label' => 'Название'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {

        $listMapper
            ->addIdentifier('name', null, array('label' => 'Название'));
    }
    public function getTemplate($name)
    {
        switch ($name) {
            case 'create':
                return "CRUDCourse/create.html.twig";
                break;
            case 'edit':
                return "CRUDCourse/edit.html.twig";
                break;
            case 'outer_list_rows_mosaic':
                return "CRUDCourse/list_outer_rows_mosaic.html.twig";
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
    public function getExportFormats()
    {
        return null;
    }
}