<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 30.08.17
 * Time: 4:09
 */

namespace AppBundle\Admin;

use AppBundle\Form\ImageType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class EventAdmin extends AbstractAdmin
{
    public $supportsPreviewMode = true;
    public function configure()
    {
        parent::configure();
        $this->classnameLabel = "Анонсы";
    }
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('type', null, array('label' => 'Тип'))
            ->add('datetime', "sonata_type_datetime_picker", array('label' => 'Дата'))
            ->add('time', null, array('label' => 'Время'))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('type',null,array('label'=>'Тип'))
           ->add('time',null,array('label'=>'Время'))

        ;

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper->addIdentifier('type', null, array('label' => 'Тип'))
            ->add('datetime', null, array('label' => 'Дата','template' => 'CRUD/list_date.html.twig'))
            ->add('time', null, array('label' => 'Время'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'delete' => array(),
                    'edit' => array(),
                )
            ))
        ;
    }

//    public function getTemplate($name)
//    {
//        switch ($name) {
//            case 'create':
//                return "CRUDVideo/edit.html.twig";
//                break;
//            case 'edit':
//                return "CRUDVideo/edit.html.twig";
//                break;
//            default:
//                return parent::getTemplate($name);
//                break;
//        }
//    }
    public function getExportFormats()
    {
        return null;
    }
}