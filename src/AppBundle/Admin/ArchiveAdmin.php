<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 30.08.17
 * Time: 4:09
 */

namespace AppBundle\Admin;

use AppBundle\Form\ImageType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class ArchiveAdmin extends AbstractAdmin
{
    protected $parentAssociationMapping = 'ArchiveFile';

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }
    public function configure()
    {
        parent::configure();
        $this->classnameLabel = "Архив";
    }
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array('label' => 'Название'))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('type',null,array('label'=>'Тип'))

        ;

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper->addIdentifier('name', null, array('label' => 'Название'))

        ;
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                return "CRUD/base_list.html.twig";
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
    public function getExportFormats()
    {
        return null;
    }

}