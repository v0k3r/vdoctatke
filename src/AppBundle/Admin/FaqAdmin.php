<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 30.08.17
 * Time: 4:09
 */

namespace AppBundle\Admin;

use AppBundle\Form\ImageType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class FaqAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('question', null, array('label' => 'Вопрос'))
            ->add('answer', null, array('label' => 'Ответ'));

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('question', null, array('label' => 'Вопрос'))
            ->add('answer',null, array('label' => 'Ответ'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);
        $listMapper->addIdentifier('question', null, array('label' => 'Вопрос','template' => 'CRUDFaq/list_question.html.twig'))
            ->addIdentifier('answer', null,array('label' => 'Ответ','template' => 'CRUDFaq/list_answer.html.twig'));
    }
    public function getTemplate($name)
    {
        switch ($name) {
            case 'create':
                return "CRUDFaq/edit.html.twig";
                break;
            case 'edit':
                return "CRUDFaq/edit.html.twig";
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
    public function getExportFormats()
    {
        return null;
    }
}