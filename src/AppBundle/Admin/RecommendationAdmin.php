<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 30.08.17
 * Time: 4:09
 */

namespace AppBundle\Admin;

use AppBundle\Form\ImageType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Knp\Menu\ItemInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
class RecommendationAdmin extends AbstractAdmin
{

    public function buildBreadcrumbs($action, MenuItemInterface $menu = null) {
        $breadCrumb =  parent::buildBreadcrumbs($action, $menu);

        if ($action == 'list' && $this->hasSubject()) {
            $breadCrumb->setName(
               'Список'
            );
        }

        return $breadCrumb;
    }
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('firstName', null, array('label' => 'Имя'))
            ->add('city', null, array('label' => 'Город'))
            ->add('photoPath')
            ->add('uploadImage',ImageType::class, array('required' => false))
            ->add('text', null, array('label' => 'Отзыв'));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('firstName', null, array('label' => 'Имя'))
            ->add('city', null, array('label' => 'Город'))
            ->add('text', null, array('label' => 'Текст отзыва'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {

        $listMapper
            ->addIdentifier('firstName', null, array('label' => 'Имя'))
            ->add('city', null, array('label' => 'Город'))
            ->add('text', null, array('label' => 'Текст отзыва'))
        ;
    }
    public function getTemplate($name)
    {
        switch ($name) {
            case 'create':
                return "CRUDRecommendation/edit.html.twig";
                break;
            case 'edit':
                return "CRUDRecommendation/edit.html.twig";
                break;
            case 'outer_list_rows_mosaic':
                return "CRUDRecommendation/list_outer_rows_mosaic.html.twig";
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
    public function getExportFormats()
    {
        return null;
    }
}