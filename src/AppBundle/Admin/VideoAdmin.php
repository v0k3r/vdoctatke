<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 30.08.17
 * Time: 4:09
 */

namespace AppBundle\Admin;

use AppBundle\Form\ImageType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class VideoAdmin extends AbstractAdmin
{

    public function configure()
    {
        parent::configure();
        $this->classnameLabel = "Видео";
    }
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('videoId', null, array('label' => 'Ссылка'))
            ->add('videoText', null, array('label' => 'Ссылка'))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

    }
    protected function configureRoutes(RouteCollection $collection)
    {
        // to remove a single route
        $collection->remove('delete');
        $collection->remove('create');
        // OR remove all route except named ones

    }
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->addIdentifier('type', null, array('label' => 'Тип','template' => 'CRUDVideo/list_type.html.twig'));
    }
    public function getTemplate($name)
    {
        switch ($name) {
            case 'create':
                return "CRUDVideo/edit.html.twig";
                break;
            case 'edit':
                return "CRUDVideo/edit.html.twig";
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
    public function getExportFormats()
    {
        return null;
    }
}