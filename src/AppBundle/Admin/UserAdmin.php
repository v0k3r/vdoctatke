<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 30.08.17
 * Time: 4:09
 */

namespace AppBundle\Admin;

use AppBundle\Form\ImageType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class UserAdmin extends AbstractAdmin
{
    public function getExportFormats()
    {
        return null;
    }

    protected $baseRoutePattern = 'user';
//    public $supportsPreviewMode = true;
    public function configureBatchActions($actions)
    {
        if (
            $this->hasRoute('edit') && $this->hasAccess('edit') &&
            $this->hasRoute('delete') && $this->hasAccess('delete')
        ) {
            $actions['email'] = array(
                'ask_confirmation' => true
            );

        }

        return $actions;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
//        // to remove a single route
        $collection->remove('create');
        // OR remove all route except named ones
//        $collection->clearExcept(array('list', 'show'));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $object=$this->getSubject();
        $container = $this->getConfigurationPool()->getContainer();
        $roles = $container->getParameter('security.role_hierarchy.roles');
        $rolesChoices = self::flattenRoles($roles);
        $formMapper->add('email')
            ->add('first_name', null, array('label' => 'Имя'))
            ->add('last_name')
            ->add('tel')
            ->add('whatsup')
            ->add('telegram')
            ->add('city')
            ->add('country')
            ->add('vk')
            ->add('vk')
            ->add('sureone_id')
            ->add('password',null, array('required' => false))


            ->add('photoPath')
            ->add('uploadAvatar',ImageType::class, array('required' => false))
            ->add('landingName', null, array('label' => 'Имя'))
        ;

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('email')
            ->add('first_name',null, array('label' => 'Имя'))
            ->add('last_name',null, array('label' => 'Фамилия'))
            ->add('landingName',null, array('label' => 'Персональная страница'))
            ->add('city', null, array('label' => 'Город'))
            ->add('vk', null, array('label' => 'vk'))
            ->add('tel', null, array('label' => 'Телефон'))

        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper

            ->addIdentifier('first_name', null, array('label' => 'Имя'))
            ->add('last_name', null, array('label' => 'Фамилия'))
            ->add('city', null, array('label' => 'Город'))
            ->add('email', null, array('label' => 'Email'))
            ->add('tel', null, array('label' => 'Телефон'))
            ->add('sureone_id', null, array('label' => 'TIkiId'))
            ->add('vk', null, array('label' => 'vk'))
            ->add('landingName', null, array('label' => 'Персональная страница'))
        ;
    }


    public function getTemplate($name)
    {
        switch ($name) {
            case 'batch_confirmation':
                return "CRUD/batch_confirmation.html.twig";
                break;
            case 'outer_list_rows_mosaic':
                return "CRUD/list_outer_rows_mosaic.html.twig";
                break;
            case 'edit':
                return "CRUD/edit.html.twig";
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    protected static function flattenRoles($rolesHierarchy)
    {
        $flatRoles = array();
        foreach ($rolesHierarchy as $roles) {

            if (empty($roles)) {
                continue;
            }

            foreach ($roles as $role) {
                if (!isset($flatRoles[$role])) {
                    switch ($role) {
                        case "ROLE_ADMIN":
                            $flatRoles["Админ"] = $role;
                            break;
                        case "ROLE_USER":
                            $flatRoles["Неподтвержден"] = $role;
                            break;
                        case "ROLE_GUEST":
                            $flatRoles["Неполная регистрация"] = $role;
                            break;
                        case "ROLE_PARTNER":
                            $flatRoles["Подтвержден"] = $role;
                            break;
                        case "ROLE_NEW_PARTNER":
                            $flatRoles["Еще не создал лэндинг"] = $role;
                            break;
                        case "ROLE_BANNED":
                            $flatRoles["Забанен"] = $role;
                            break;
                    }
                }
            }
        }

        return $flatRoles;
    }
}