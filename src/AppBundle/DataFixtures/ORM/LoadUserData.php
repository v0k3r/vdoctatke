<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\DependencyInjection\ContainerInterface;






class LoadUserData implements FixtureInterface,ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {



        $user1 = new User();
        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user1, '123456');
        $roles=['ROLE_ADMIN'];
        $user1->setFirstName('Эмма')->setLastName('Мустафина')->setEmail('admin@admin.ru')->setRoles($roles)->setPassword($password)->setLandingName('lucky');
        $manager->persist($user1);
        $user2 = new User();
        $user2->setFirstName('Иван')->setLastName('Алешкин')->setEmail('admin1@admin.ru')->setRoles($roles)->setPassword($password)->setParent($user1);
        $manager->persist($user2);
        $user=new User();
        $user->setFirstName('Вадим')->setLastName('Петренко')->setEmail('admin11@admin.ru')->setRoles($roles)->setPassword($password)->setParent($user2);
        $manager->persist($user);
        $user=new User();
        $user->setFirstName('Александр')->setLastName('Богданов')->setEmail('admin12@admin.ru')->setRoles($roles)->setPassword($password)->setParent($user2);
        $manager->persist($user);
        $user=new User();
        $user->setFirstName('Ваша')->setLastName('поддержка')->setEmail('support@admin.ru')->setRoles($roles)->setPassword($password);
        $manager->persist($user);
        $manager->flush();
    }
}