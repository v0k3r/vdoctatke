<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 17.06.17
 * Time: 8:44
 */
namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use AppBundle\Form\DataTransformer\UserToTextTransformer;

class FullStepsRegistrationType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name',TextType::class,[
                'required'=>true
            ])
            ->add('last_name',TextType::class)
            ->add('email',TextType::class)
            ->add('vk',TextType::class)
            ->add('tel',TextType::class)
            ->add('whatsup',TextType::class)
            ->add('last_name',TextType::class)
            ->add('sureone_id',TextType::class)
            ->add('city',TextType::class)
            ->add('country',TextType::class)
            ->add('telegram',TextType::class)
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password'),
            ))
            ->remove('username')
            ->setMethod("POST");



    }
}