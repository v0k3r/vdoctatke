<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 26.06.17
 * Time: 21:53
 */
namespace AppBundle\Form\DataTransformer;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class UserToTextTransformer implements DataTransformerInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Transforms an user-object to a string .
     *
     * @param  User|null $user
     * @return string
     */
    public function transform($user)
    {
        if (null === $user) {
            return '';
        }

        return $user->getLandingName();
    }

    /**
     * Transforms a string to an user-object.
     *
     * @param  string $landingName
     * @return User|null
     * @throws TransformationFailedException if object is not found.
     */
    public function reverseTransform($landingName)
    {

        if (!$landingName) {
            return;
        }

        $user = $this->em
            ->getRepository('AppBundle:User')
            // query for the issue with this id
            ->findOneByLandingName($landingName);

        if (null === $user) {
            throw new TransformationFailedException(sprintf(
                'Не существует юзера с таким id: "%s" ',
                $landingName
            ));
        }

        return $user;
    }
}