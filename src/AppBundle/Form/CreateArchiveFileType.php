<?php
/**
 * IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH 
 * 
 * The following source code is PROPRIETARY AND CONFIDENTIAL. Use of this source code 
 * is governed by the IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. Non-Disclosure Agreement 
 * previously entered between you and IAT INNOVATIVE ADVERTISING TECHNOLOGIES GMBH. 
 * 
 * By accessing, using, copying, modifying or distributing this software, you acknowledge 
 * that you have been informed of your obligations under the Agreement and agree 
 * to abide by those obligations. 
 * 
 * @author vladislav <vladislav.baymurzin@i-a-t.net>  
 */


namespace AppBundle\Form;


use AppBundle\DTO\CreateArchiveFile;
use AppBundle\Entity\ArchiveFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateArchiveFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('text');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ArchiveFile::class
        ));
    }
}