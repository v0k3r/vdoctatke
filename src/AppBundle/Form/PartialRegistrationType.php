<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 17.06.17
 * Time: 8:44
 */
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;

class PartialRegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('first_name')
            ->add('email')
            ->add('password', PasswordType::class)
            ->setMethod("POST");
    }
}