<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 12.09.17
 * Time: 17:05
 */

namespace AppBundle\Form;

use AppBundle\Entity\ClientArchiveFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateClientArchiveFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('text');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ClientArchiveFile::class
        ));
    }
}