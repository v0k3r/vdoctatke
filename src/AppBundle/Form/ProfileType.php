<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 17.06.17
 * Time: 8:44
 */
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('uploadAvatar',FileType::class,array('data_class' => null,'required'=>false))
            ->add('vk')
            ->add('tel')
            ->add('whatsup')
            ->add('telegram')
            ->add('sureoneId')
            ->add('landingName')
            ->add('country')
            ->add('city')
            ->setMethod("POST");

    }
    public function configureOptions(OptionsResolver $resolver)
    {

    }
}