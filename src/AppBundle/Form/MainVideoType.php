<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 28.08.17
 * Time: 19:49
 */
namespace AppBundle\Form;

use AppBundle\Entity\MainVideo;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MainVideoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('videoText')
            ->add('videoId', TextareaType::class);
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => MainVideo::class,
        ));
    }
}