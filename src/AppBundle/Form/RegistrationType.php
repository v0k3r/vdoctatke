<?php
/**
 * Created by PhpStorm.
 * User: kolya
 * Date: 17.06.17
 * Time: 8:44
 */
namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use AppBundle\Form\DataTransformer\UserToTextTransformer;

class RegistrationType extends AbstractType
{
    private $transformer;

    public function __construct(UserToTextTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('first_name',TextType::class,[
            'required'=>true
        ])

            ->add('email',TextType::class,[
                'required'=>true,
                'invalid_message' => 'Неверный формат email',
            ])
            ->add('password', PasswordType::class,[
                'required'=>true,
                'invalid_message' => 'Пароль не должен быть пустым',
            ])
            ->add('parent', TextType::class, [
                'required'=>true,
                'invalid_message' => 'Неверный идентификатор пригласителя',
            ])
            ->setMethod("POST");

        $builder->get('parent')
            ->addModelTransformer($this->transformer);

    }
}