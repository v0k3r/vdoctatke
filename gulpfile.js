var gulp 		 = require('gulp'),
    jade 		 = require('gulp-jade'),
    sass 		 = require('gulp-sass'),
    concat       = require('gulp-concat'),
    uglify       = require('gulp-uglifyjs'),
    cssnano      = require('gulp-cssnano'),
    rename       = require('gulp-rename'),
    del          = require('del'),
    imagemin     = require('gulp-imagemin'),
    pngquant     = require('imagemin-pngquant'),
    cache        = require('gulp-cache'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync  = require('browser-sync'),
    watch        = require('gulp-watch');

var publicPath   = 'app/Resources/public/',
    destPath     = 'web/';

gulp.task('img', function(){
    return gulp.src(publicPath + 'img/**/*')
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlagins: [{removeViewBox: false}],
            une: [pngquant()]
        })))
        .pipe(gulp.dest(destPath + 'img'));
});

gulp.task('clear', function(){
    return cache.clearAll();
});

gulp.task('tt', function () {
    return gulp.src(publicPath + 'js/password-score-options.js')
        .pipe(uglify())
        .pipe(gulp.dest(publicPath))
})

gulp.task('clean', function(){
    return del.sync([
        'web/css',
        'web/js',
        'web/fonts',
        'web/img',
        'web/audio',
        'web/plugins',
        'web/icons',
        'web/assets'
    ]);
});

gulp.task('buildJs', function () {
    return gulp.src(publicPath + 'components/**/*')
        .pipe(gulp.dest(destPath + 'components'));
});

gulp.task('plugins', function () {
    return gulp.src(publicPath + 'plugins/**/*')
        .pipe(gulp.dest(destPath + 'plugins'));
});

gulp.task('buildAssets', function () {
    return gulp.src(publicPath + 'js/**/*')
        .pipe(gulp.dest(destPath + 'js'));
});

gulp.task('fonts', function () {
    return gulp.src(publicPath + 'fonts/**/*')
        .pipe(gulp.dest(destPath + 'fonts'));
});

gulp.task('styles', function () {
    return gulp.src(publicPath + 'css/**/*')
        .pipe(gulp.dest(destPath + 'css'));
});

gulp.task('assets', function () {
    return gulp.src(publicPath + 'assets/**/*')
        .pipe(gulp.dest(destPath + 'assets'));
});
gulp.task('icons', function () {
    return gulp.src(publicPath + 'icons/**/*')
        .pipe(gulp.dest(destPath + 'icons'));
});
gulp.task('build', ['clean', 'img', 'buildJs', 'fonts', 'buildAssets', 'styles', 'plugins', 'icons', 'assets'], function () {

});