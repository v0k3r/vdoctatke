<?php

namespace Application\Migrations;

use AppBundle\Entity\Course;
use AppBundle\Entity\User;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170729104711 extends AbstractMigration implements ContainerAwareInterface
{

    private $container;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $em = $this->container->get('doctrine.orm.entity_manager');

        $course=new Course();
        $course->setName('ШАГ 7. Личная встреча (Демонстрация).')->setVideoId('It_BxKK-0os');
        $em->persist($course);
        $course=new Course();
        $course->setName('ШАГ 8. Новый клиент.')->setVideoId('fJM7matoq_8');
        $em->persist($course);
        $course=new Course();
        $course->setName('ШАГ 9. Новый партнёр.')->setVideoId('f2ueaCkr6bU');
        $em->persist($course);

        $course=new Course();
        $course->setName('ШАГ 10. Статистика и учёт.')->setVideoId('mrCuvzt-UxA');
        $em->persist($course);
        $course=new Course();
        $course->setName('Бонусный урок. Обретение неявных преимуществ в бизнесе.')->setVideoId('qduLs9t6ny4');
        $em->persist($course);
        $course=new Course();
        $course->setName('ШАГ 12.')->setVideoId('AkEgX6i6n2k');
        $em->persist($course);

        $em->flush();

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
        $this->container = $container;
    }
}
