<?php

namespace Application\Migrations;

use AppBundle\Entity\BusinessOnlineCourse;
use AppBundle\Entity\TrainingCourse;
use AppBundle\Entity\ZorikTrainingCourse;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180610132815 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $course=new ZorikTrainingCourse();
        $course->setName('1. Обзор работы сканера')->setVideoId('Z-b2lN3fMo0')->setPhotoPath('1.jpg');
        $em->persist($course);

        $course=new ZorikTrainingCourse();
        $course->setName('2. Установка браузера, VPN, расширений')->setVideoId('qoh9MjQQhQk')->setPhotoPath('2.jpg');
        $em->persist($course);

        $course=new ZorikTrainingCourse();
        $course->setName('3. Установка расширения 1click')->setVideoId('H7wiceyE7QI')->setPhotoPath('3.jpg');
        $em->persist($course);

        $course=new ZorikTrainingCourse();
        $course->setName('4. Настройка VPS')->setVideoId('St5JuF0tpS0')->setPhotoPath('4.jpg');
        $em->persist($course);

        $course=new ZorikTrainingCourse();
        $course->setName('5. Регистрация в платежных системах Skrill и Qiwi')->setVideoId('oJOllwqSn_Q')->setPhotoPath('5.jpg');
        $em->persist($course);

        $course=new ZorikTrainingCourse();
        $course->setName('6. Регистрация в букмекерских конторах')->setVideoId('x8UjzJy3NLc')->setPhotoPath('6.jpg');
        $em->persist($course);

        $course=new ZorikTrainingCourse();
        $course->setName('7. Примеры вилок')->setVideoId('rk3dy8a4XCQ')->setPhotoPath('7.jpg');
        $em->persist($course);

        $course=new ZorikTrainingCourse();
        $course->setName('8. LIFEHACK для практиков')->setVideoId('aw4qPgvXhU0')->setPhotoPath('8.jpg');
        $em->persist($course);

        $course=new ZorikTrainingCourse();
        $em->flush();
/*
        1.  https://youtu.be/Z-b2lN3fMo0
2. https://youtu.be/qoh9MjQQhQk
3. https://youtu.be/H7wiceyE7QI
4. https://youtu.be/St5JuF0tpS0
5. https://youtu.be/oJOllwqSn_Q
6. https://youtu.be/x8UjzJy3NLc
7.  https://youtu.be/rk3dy8a4XCQ
8. https://youtu.be/aw4qPgvXhU0
*/
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
        $this->container = $container;
    }
}
