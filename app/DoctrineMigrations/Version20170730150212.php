<?php

namespace Application\Migrations;

use AppBundle\Entity\Course;
use AppBundle\Entity\TrainingCourse;
use AppBundle\Entity\User;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170730150212 extends AbstractMigration implements ContainerAwareInterface
{

    private $container;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $em = $this->container->get('doctrine.orm.entity_manager');

        $course=new TrainingCourse();
        $course->setName('ШАГ 1. Активация сканера. Как зайти на BigBet.pro.')->setVideoId('3oZ6BjJCUb0');
        $em->persist($course);
        $course=new TrainingCourse();
        $course->setName('ШАГ 2. Установка браузера и VPN.')->setVideoId('rjJJV5jQBZA');
        $em->persist($course);
        $course=new TrainingCourse();
        $course->setName('ШАГ 3. Обзор сканера TIKI.')->setVideoId('WAEBLl4ECfQ');
        $em->persist($course);
        $course=new TrainingCourse();
        $course->setName('ШАГ 4. Работа с BB Helper.')->setVideoId('6Ya0Fp1C0e4');
        $em->persist($course);
        $course=new TrainingCourse();
        $course->setName('ШАГ 5. Платежные системы.')->setVideoId('lmMo8TkMyjE');
        $em->persist($course);
        $course=new TrainingCourse();
        $course->setName('ШАГ 6. Регистрация в БК.')->setVideoId('ir7zbqzMy0M');
        $em->persist($course);
        $course=new TrainingCourse();
        $course->setName('ШАГ 7. Стратегии «вилкования».')->setVideoId('db6cBQoQAXc');
        $em->persist($course);
        $course=new TrainingCourse();
        $course->setName('ШАГ 8. Техника безопасности.')->setVideoId('BK8IDFgb_3g');
        $em->persist($course);
        $course=new TrainingCourse();
        $course->setName('ШАГ 9. Практика, примеры работы со сканером.')->setVideoId('iXoXQ2zCHsU');
        $em->persist($course);
        $course=new TrainingCourse();
        $course->setName('ШАГ 10. Завершающие рекомендации.')->setVideoId('d_uUqb_TSiU');
        $em->persist($course);
        $course=new TrainingCourse();
        $course->setName('ШАГ 11. Что делать, если ваш аккаунт «порезали»?')->setVideoId('V0041zVeCeQ');
        $em->persist($course);

        $em->flush();

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
        $this->container = $container;
    }
}
