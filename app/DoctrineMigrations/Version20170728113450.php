<?php

namespace Application\Migrations;

use AppBundle\Entity\Course;
use AppBundle\Entity\User;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170728113450 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $em = $this->container->get('doctrine.orm.entity_manager');
        $user=new User();
        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user, '123456');
        $roles=['ROLE_ADMIN'];
        $user->setFirstName('Ваша')->setLastName('поддержка')->setEmail('support@admin.ru')->setRoles($roles)->setPassword($password);
        $em->persist($user);
        $em->flush();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
        $this->container = $container;
    }
}
