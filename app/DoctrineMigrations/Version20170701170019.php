<?php

namespace Application\Migrations;

use AppBundle\Entity\Course;
use AppBundle\Entity\User;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170701170019 extends AbstractMigration implements ContainerAwareInterface
{

    private $container;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $em = $this->container->get('doctrine.orm.entity_manager');
        $user1 = new User();
        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user1, '123456');
        $roles=['ROLE_ADMIN'];
        $user1->setFirstName('Эмма')->setLastName('Мустафина')->setEmail('admin@admin.ru')->setRoles($roles)->setPassword($password)->setLandingName('lucky');
        $em->persist($user1);

        $course=new Course();
        $course->setName('ШАГ 1. Правила игры.')->setVideoId('SRG1u6RPMpM');
        $em->persist($course);
        $course=new Course();
        $course->setName('ШАГ 2. Выбор бизнес-пакета.')->setVideoId('HU8o3v-KfeA');
        $em->persist($course);
        $course=new Course();
        $course->setName('ШАГ 3. Целеполагание.')->setVideoId('60F3M4PNILo');
        $em->persist($course);

        $course=new Course();
        $course->setName('ШАГ 4. Результат по продукту как основной инструмент продвижения.')->setVideoId('iZKggZaKy-Y');
        $em->persist($course);
        $course=new Course();
        $course->setName('ШАГ 5. Список контактов.')->setVideoId('-QfwrIlVrCk');
        $em->persist($course);
        $course=new Course();
        $course->setName('ШАГ 6. Приглашения.')->setVideoId('BSC1IpP3ETQ');
        $em->persist($course);

        $em->flush();

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
        $this->container = $container;
    }
}
