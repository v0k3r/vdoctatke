<?php

namespace Application\Migrations;

use AppBundle\Entity\TypedVideoCourse;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181224164014 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

        /**
         * @var EntityManager $em
         */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $course=new TypedVideoCourse();
        $course->setName('1. Привествие')->setVideoId('VUA5gSVy4QI')->setPhotoPath('1.jpg')->setType("");
        $em->persist($course);

        $course=new TypedVideoCourse();
        $course->setName('2. Как зайти в экспресс')->setVideoId('1sLCXxbXbu4')->setPhotoPath('2.jpg')->setType("tiki_express");
        $em->persist($course);

        $course=new TypedVideoCourse();
        $course->setName('3. Какие БК нужны и регистрация в них')->setVideoId('AOO3DkXnP6g')->setPhotoPath('3.jpg')->setType("tiki_express");
        $em->persist($course);

        $course=new TypedVideoCourse();
        $course->setName('4. Обзор функционала')->setVideoId('o_N53TOjRA0')->setPhotoPath('4.jpg')->setType("tiki_express");
        $em->persist($course);

        $course=new TypedVideoCourse();
        $course->setName('5. Ответы на вопросы')->setVideoId('SMlyN-E2KWU')->setPhotoPath('5.jpg')->setType("tiki_express");
        $em->persist($course);

        $course=new TypedVideoCourse();
        $course->setName('6.1 Примеры пользования')->setVideoId('QRahUbn25DI')->setPhotoPath('6.1.jpg')->setType("tiki_express");
        $em->persist($course);

        $course=new TypedVideoCourse();
        $course->setName('6.2 Примеры пользования')->setVideoId('M0p3URN7TA4')->setPhotoPath('6.2.jpg')->setType("tiki_express");
        $em->persist($course);

        $course=new TypedVideoCourse();
        $course->setName('6.3 Примеры пользования')->setVideoId('_m7CaR4hwLU ')->setPhotoPath('6.3.jpg')->setType("tiki_express");
        $em->persist($course);

        $course=new TypedVideoCourse();
        $course->setName('7. Как находить матчи, исходы, как собирать экспрессы')->setVideoId('7fiBvS6YcKs')->setPhotoPath('7.jpg')->setType("tiki_express");
        $em->persist($course);

        $course=new TypedVideoCourse();
        $course->setName('8. Типы ставок и их обозначения')->setVideoId('Tkl9CIkM0Wc')->setPhotoPath('8.jpg')->setType("tiki_express");
        $em->persist($course);

        $course=new TypedVideoCourse();
        $course->setName('9. VPN и VPS')->setVideoId('LYqoGsJj6w4')->setPhotoPath('9.jpg')->setType("tiki_express");
        $em->persist($course);

        $em->flush();


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
        $this->container = $container;
    }
}
