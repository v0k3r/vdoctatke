<?php

namespace Application\Migrations;

use AppBundle\Entity\BusinessOnlineCourse;
use AppBundle\Entity\TrainingCourse;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171230113028 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $course=new BusinessOnlineCourse();
        $course->setName('Взрывной рост Всетях VK.')->setVideoId('gVXxiF_0LIw');
        $em->persist($course);
        $course=new BusinessOnlineCourse();
        $course->setName('Взрывной рост Всетях INSTAGRAM.')->setVideoId('g4V9S03Byrk');
        $em->persist($course);
        $course=new BusinessOnlineCourse();
        $course->setName('Взрывной рост Всетях YOUTUBE.')->setVideoId('1-Om9ciSHhY');
        $em->persist($course);
        $course=new BusinessOnlineCourse();
        $course->setName('Взрывной рост Всетях TELEGRAM.')->setVideoId('Bqvvw0H1Uhc');
        $em->persist($course);
        $em->flush();

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
        $this->container = $container;
    }
}
