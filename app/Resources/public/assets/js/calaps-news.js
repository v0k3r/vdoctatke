

        $(document).ready(function () {
            var maxheight = 310;
            var showText = "Смотреть полностью";
            var hideText = "Свернуть";

            $('.calaps-news').each(function () {
                var text = $(this);
                if (text.height() > maxheight) {
                    text.css({'overflow': 'hidden', 'height': maxheight + 'px'});

                    var link = $('<button type="button" class="btn btn-flat btn-default-dark">Смотреть полностью</button>');
                    var linkDiv = $('<div class="card-body style-default-bright"></div>');
                    linkDiv.append(link);
                    $(this).after(linkDiv);

                    link.click(function (event) {
                        event.preventDefault();
                        if (text.height() > maxheight) {
                            $(this).html(showText);
                            text.css('height', maxheight + 'px');
                        } else {
                            $(this).html(hideText);
                            text.css('height', 'auto');
                        }
                    });
                }
            });
        });
        

    