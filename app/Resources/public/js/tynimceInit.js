/**
 * Created by kolya on 11.07.17.
 */
tinymce.init({
    selector: '#news_body',
    setup: function (editor) {
        editor.on('change', function (e) {
            editor.save();
        });
    },
    language: 'ru',
    language_url: '/js/langs/ru.js',
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste imagetools"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | sizeselect |  fontsizeselect",
    imagetools_cors_hosts: ['www.tinymce.com', 'codepen.io'],
    content_css: [
        '//www.tinymce.com/css/codepen.min.css'
    ]
});