/**
 * Created by Panda on 28.06.2017.
 */

function enbleButton() {
    $('.disabled-next-btn').removeAttr('disabled');
}
function ajaxCall() {
    $.ajax({
        type: 'POST',
        url: '/cabinet/stepsSucces',
        data: 'isEnded=true',
        success: function (data) {
            $('.disabled-next-btn').removeAttr('disabled');
        }
    });
}
var flag=true;
function timeFormat(time){
    if (Math.floor(time)<=0) {
        if (flag) {
           ajaxCall();
            flag = false;
        }
        return 'Вы можете перейти к другому шагу';

    }
    console.log(time);
    function num(val){
        val = Math.floor(val);
        return val < 10 ? '0' + val : val;
    }


        var sec = time
            , minutes = sec / 60 % 60

            ;

        return num(minutes) + ":" + num(sec%60);

}

function onTrackedVideoFrame(currentTime, duration) {
    var t=null;
    $(".video-time").text(timeFormat(380-currentTime));
}



$(function () {
    $('#modalVideo').on('ended', function () {
        $('.disabled-next-btn').removeAttr('disabled');
        // $('.disabled-next-btn').on('click', function () {
        //     window.location.replace('3.html');
        // });
    });

});

$(document).ready(function () {

    $("#modalVideo").on(
        "timeupdate",
        function (event) {
            onTrackedVideoFrame(this.currentTime, this.duration);
        });
});

$(function () {
    if ($('#modalVideo').length) {
        var player = videojs('modalVideo'),
            modal = $('#videoModal');

        player.ready(function () {
            var myPlayer = this;
            myPlayer.src({type: 'video/mp4', src: '/img/10.mp4'});
        });

        modal
            .on('show.bs.modal', function () {
                if (player) player.play();
            })
            .on('hide.bs.modal', function () {
                if (player) player.pause();
            });

        player.on('ended', function () {
            $.ajax({
                type: 'POST',
                url: '/cabinet/stepsSucces',
                data: 'isEnded=true',
                success: function (data) {
                    $('.disabled-next-btn').removeAttr('disabled');
                }
            });
        });
    }

    if ($('#modalVideo2').length) {
        var player2 = videojs('modalVideo2'),
            modal = $('#videoModal');


        player2.ready(function () {
            var myPlayer2 = this;
            myPlayer2.src({type: 'video/mp4', src: '/img/10.mp4'});
        });

        modal
            .on('show.bs.modal', function () {
                player2.play();
            })
            .on('hide.bs.modal', function () {
                player2.pause();
            })
        player2.on('ended', function () {
            $.ajax({
                type: 'POST',
                url: '/cabinet/stepsSucces',
                data: 'isEnded=true',
                success: function (data) {
                    $('.disabled-next-btn').removeAttr('disabled');
                }
            });
        });
    }

});
//Если хочешь убрать блокировку кнопки то раскоментируй это
/*
 * $(function () {
 *   $('.disabled-next-btn').removeAttr('disabled');
 $('.disabled-next-btn').on('click', function () {
 window.location.replace('3.html');
 });
 * }*/