/**
 * Created by Panda on 17.06.2017.
 */
(function () {
    var originalAddClassMethod = jQuery.fn.addClass;

    jQuery.fn.addClass = function () {
        var result = originalAddClassMethod.apply(this, arguments);

        jQuery(this).trigger('cssClassChanged');

        return result;
    }
})();
$(function () {
    $('.phone-mask').intlTelInput({
        onlyCountries: ["ru", "kz", "ua", "az", "by", "AM", "KG", "MD", "TJ", "TM", "UZ", "GE"],
        initialCountry: 'RU'
    });
    // $('.phone-mask').intlTelInput();
    $('.login-email-field').blur(function () {
        if ($(this).val() !== '') {
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if (pattern.test($(this).val())) {
                $('.login-error-zone').removeClass('alert-danger').addClass('alert-success')
                    .html('Поле email заполнено верно.');
            }
            else {
                $('.login-error-zone').removeClass('hidden').removeClass('alert-success')
                    .addClass('alert-danger').html('Поле email заполнено неверно.');
            }
        }
        else {
            $('.login-error-zone').removeClass('hidden').removeClass('alert-success')
                .addClass('alert-danger').html('Поле email не должно быть пустым.');
        }
    });
    $('.reg-email-field').blur(function () {
        if ($(this).val() !== '') {
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if (pattern.test($(this).val())) {
                $('.reg-error-zone').removeClass('alert-danger').addClass('alert-success')
                    .html('Поле email заполнено верно.');
            }
            else {
                $('.reg-error-zone').removeClass('hidden').removeClass('alert-success')
                    .addClass('alert-danger').html('Поле email заполнено неверно.');
            }
        }
        else {
            $('.reg-error-zone').removeClass('hidden').removeClass('alert-success')
                .addClass('alert-danger').html('Поле email не должно быть пустым.');
        }
    });
    $('.reg-firstname-field').blur(function () {
        if ($(this).val() !== '') {
            $('.reg-error-zone').addClass('hidden');
        }
        else {
            $('.reg-error-zone').removeClass('hidden').removeClass('alert-success')
                .addClass('alert-danger').html('Поле имя не должно быть пустым.');
        }
    });
    $('.reg-lastname-field').blur(function () {
        if ($(this).val() !== '') {
            $('.reg-error-zone').addClass('hidden');
        }
        else {
            $('.reg-error-zone').removeClass('hidden').removeClass('alert-success')
                .addClass('alert-danger').html('Поле фамилия не должно быть пустым.');
        }
    });
    $('.repassword-field').change(function () {
        var value = $('.password-field').val();
        if ($(this).val() !== value) {
            $('.reg-error-zone').removeClass('hidden').removeClass('alert-success')
                .addClass('alert-danger').html('Пароль и подтверждение пароля должны совпадать.');
        }
        else {
            $('.reg-error-zone').addClass('hidden');
        }
    });
    $('.vk-field').blur(function (event) {
        if (String($(this).val()).length < 14) {
            $(this).attr('value', 'http://vk.com/');
        }
    });
});

$(document).ready(function () {


    $('.vk-field').attr('value', 'http:/vk.com/');
    $('.password-checker').strengthMeter('progressBar', {
        container: $('.password-checker + .progress-container')
    });
    $('.progress-container > .progress > .progress-bar').bind('cssClassChanged', function () {

        $('.progress-container').removeAttr('hidden');

        if ($(this).hasClass('progress-bar-danger')) {
            $('.progress-result').html('Плохой').addClass('danger');
        }
        if ($(this).hasClass('progress-bar-warning')) {
            $('.progress-result').html('Обычный').addClass('warning');
        }
        if ($(this).hasClass('progress-bar-success')) {
            $('.progress-result').html('Хороший').addClass('success');
        }
    });

    // $('#registration_password_first').focusout(function () {
    //     $('#registration_password_second').focusout(function () {
    //         $('.progress-container').attr('hidden', 'hidden');
    //         $('.progress-result').html('');
    //     })
    // });
});


//Show registration modal form after errors
$(function () {
    if ($('div.regFormError').length && $('div#registerModalForm').length) {
        $('div#registerModalForm').modal('show');
    }
});
//Show login modal form after errors
$(function () {
    if ($('div.loginFormError').length && $('div#loginModalForm').length) {
        $('div#loginModalForm').modal('show');
    }
});