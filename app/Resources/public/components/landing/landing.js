/**
 * Created by Panda on 22.06.2017.
 */
(function(){
    var originalAddClassMethod = jQuery.fn.addClass;

    jQuery.fn.addClass = function(){
        var result = originalAddClassMethod.apply( this, arguments );

        jQuery(this).trigger('cssClassChanged');

        return result;
    }
})();

$(document).ready(function(){

    $('.password-checker').strengthMeter('progressBar',{
        container: $('.password-checker + .progress-container')
    });
    $('.progress-container > .progress > .progress-bar').bind('cssClassChanged', function () {
        $('.progress-container').removeAttr('hidden');
        if( $(this).hasClass('progress-bar-danger') ) {
            $('.progress-result').html('Плохой').addClass('danger');
        }
        if( $(this).hasClass('progress-bar-warning') ) {
            $('.progress-result').html('Обычный').addClass('warning');
        }
        if( $(this).hasClass('progress-bar-success') ) {
            $('.progress-result').html('Хороший').addClass('success');
        }
    });

    $('.preloader').addClass('loaded');
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        autoplay:true,
        nav:false,
        navText:['<','>'],
        dots:false,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            768: {
                items: 3,
                nav: false
            }
        }
    });
});
