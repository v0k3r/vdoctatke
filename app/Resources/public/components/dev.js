$(function () {
    $(function () {
        if ($('div.regFormError').length  && $('div#registerModalForm').length) {
            $('div#registerModalForm').modal('show');
        }
        if ($('div.loginFormError').length  && $('div#loginModalForm').length) {
            $('div#loginModalForm').modal('show');
        }
    });

    $("#tree").jstree({
        core: {
            data: {
                url: $("#tree").data('url')
            }
        },
        "plugins" : [ "search",'wholerow' ]
    }).on('select_node.jstree', function (e, data) {
        var id = data.node.id;
        $.ajax({
            type: 'get',
            url: '/cabinet/getuser/' + id,
        }).done(function (data) {
            $('div.strcture-info').replaceWith(data.view);
        })
    })
    var to = false;
    $('#search').keyup(function () {
        if(to) { clearTimeout(to); }
        to = setTimeout(function () {
            var v = $('#search').val();
            $('#tree').jstree(true).search(v);
        }, 250);
    });


});



//Show registration modal form after errors
$(function () {
    if ($('div.reg-error-zone').length  && $('div#registerModalForm').length) {
        $('div#registerModalForm').modal('show');
    }
});
//Show login modal form after errors
$(function () {
    if ($('div.loginFormError').length  && $('div#loginModalForm').length) {
        $('div#loginModalForm').modal('show');
    }
});