/**
 * Created by Panda on 19.06.2017.
 */
(function(){
    var originalAddClassMethod = jQuery.fn.addClass;

    jQuery.fn.addClass = function(){
        var result = originalAddClassMethod.apply( this, arguments );

        jQuery(this).trigger('cssClassChanged');

        return result;
    }
})();
$(function () {
    $('.phone-mask').intlTelInput({
        onlyCountries: ["ru", "kz", "ua", "az", "by", "AM", "KG", "MD", "TJ", "TM", "UZ", "GE"],
        initialCountry: 'RU'
    });

    $('.repassword-field').blur(function () {
        var value = $('.password-field').val();
        if ($(this).val() !== value) {
            $('.edit-error-zone').removeClass('hidden').removeClass('alert-success')
                .addClass('alert-danger').html('Пароль и подтверждение пароля должны совпадать.');
        }
        else {
            $('.edit-error-zone').addClass('hidden');
        }
    });

});
$(document).ready(function () {
    $('.password-checker').strengthMeter('progressBar',{
        container: $('.password-checker + .progress-container')
    });
    $('.progress-container > .progress > .progress-bar').bind('cssClassChanged', function () {
        $('.progress-container').removeAttr('hidden');
        if( $(this).hasClass('progress-bar-danger') ) {
            $('.progress-result').html('Плохой').addClass('danger');
        }
        if( $(this).hasClass('progress-bar-warning') ) {
            $('.progress-result').html('Обычный').addClass('warning');
        }
        if( $(this).hasClass('progress-bar-success') ) {
            $('.progress-result').html('Хороший').addClass('success');
        }
    });

    $('.collapse-dropdown-button').on('click', function () {
        $(this).toggleClass('open-collapse');
    });
    $('[data-toggle="tooltip"]').tooltip();

});